package com.sahtek.mydoctor.models;

/**
 * Created by nasri on 13/01/2018.
 */

public class Promotions {

    private String id = "";
    private String uuid = "";
    private String profile_id = "";
    private String pic = "";
    private String category = "";
    private String start_date = "";
    private String end_date = "";
    private String title = "";
    private String body = "";
    private String publish = "";
    private String authorname = "user user";

    public Promotions(String id, String profile_id, String category, String start_date, String end_date, String title, String body, String publish, String pic) {
        this.id = id;
        this.profile_id = profile_id;
        this.category = category;
        this.start_date = start_date;
        this.end_date = end_date;
        this.title = title;
        this.body = body;
        this.publish = publish;
        this.pic = pic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getAuthorname() {
        return authorname;
    }

    public void setAuthorname(String authorname) {
        this.authorname = authorname;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
