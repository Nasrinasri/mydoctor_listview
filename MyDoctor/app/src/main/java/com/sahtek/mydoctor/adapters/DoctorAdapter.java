package com.sahtek.mydoctor.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.fragments.MyCliniqueFragment;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.sahtek.mydoctor.utils.AppUtility.loadPhoto;


public class DoctorAdapter extends BaseAdapter {
    public boolean favoris = false;
    public MyCliniqueFragment fragment = null;
    Context _context;
    ArrayList<BusinessProfil> _listBusinessProfil;
    // int  avatarSize;


    public DoctorAdapter(Context context, ArrayList<BusinessProfil> listDoctor) {
        this._context = context;
        this._listBusinessProfil = listDoctor;
        // avatarSize = context.getResources().getDimensionPixelSize(R.dimen.layout_height_70dp);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this._listBusinessProfil.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this._listBusinessProfil.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public void add(BusinessProfil doctor) {
        this._listBusinessProfil.add(doctor);
        notifyDataSetChanged();
    }

    public void add_fisrt(BusinessProfil doctor) {
        this._listBusinessProfil.add(0, doctor);

    }

    public void remove(BusinessProfil doctor) {
        this._listBusinessProfil.remove(doctor);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final BusinessProfil doctor = (BusinessProfil) getItem(position);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_doctor_item,
                    null);
        }


        TextView name = (TextView) convertView
                .findViewById(R.id.name);
        TextView sepliciality = (TextView) convertView
                .findViewById(R.id.sepliciality);

        TextView delete = (TextView) convertView
                .findViewById(R.id.delete);
        name.setText(doctor.getName());
        name.setSelected(true);
        sepliciality.setText(AppUtility.getType ( _context, doctor.getType()) );

        if (favoris) {
            delete.setVisibility(View.VISIBLE);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (fragment != null) {
                        RemoveFavoris(doctor);
                    }

                }
            });
        } else {
            delete.setVisibility(View.GONE);
        }

        ImageView img = convertView.findViewById(R.id.img);
        if(doctor.getUser() != null)
        { loadPhoto(_context, doctor.getUser().getAvatar(), img);}
        return convertView;
    }

    private void RemoveFavoris(final BusinessProfil profil) {

        JSONObject jdata = new JSONObject();
        String Connect_URL = AppConstants.REMOVE_FAVORIS_URL;
        try {
            jdata.put(AppConstants.profile_id, profil.getFavorisId());
        } catch (Exception ex) {
        }
        StringEntity entity;
        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.postAutorized(_context, Connect_URL, new SessionManager(_context).getAccessToken(), entity, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (errorResponse != null) {
                        Toast.makeText(_context,
                                errorResponse.toString(), Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(_context,
                                _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                .show();

                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }

                @Override
                public void onStart() {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.e("response", "response " + response.toString());
                    String rsp = ResponseParser.parseResultResponse(response);
                    String msg = ResponseParser.parseMessageResponse(response);
                    if (rsp.equals("+OK")) {
                        Toast.makeText(_context,
                                msg, Toast.LENGTH_SHORT)
                                .show();
                        if (fragment != null) {
                            fragment.profils.remove(profil);
                            notifyDataSetChanged();
                        }

                    } else {
                        Toast.makeText(_context,
                                msg, Toast.LENGTH_SHORT)
                                .show();
                    }
                }

            });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }


}