package com.sahtek.mydoctor.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.fragments.ReservationFragment;
import com.sahtek.mydoctor.models.Reservation;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by nasri on 13/01/2018.
 */

public class ReservationAdapter extends BaseAdapter {
    Context _context;
    ArrayList<Reservation> _listItems = new ArrayList<Reservation>();
    ReservationFragment fragment;

    public ReservationAdapter(Context context, ArrayList<Reservation> _listItems, ReservationFragment fragment) {
        this._context = context;
        this._listItems = _listItems;
        this.fragment = fragment;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this._listItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this._listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public void DeleteItem(Reservation item) {

        this._listItems.remove(item);
        notifyDataSetChanged();

    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Reservation med = (Reservation) getItem(position);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = infalInflater.inflate(R.layout.reservation_item, null);
        }

        TextView date_value = (TextView) convertView.findViewById(R.id.date_value);
        TextView hour_value = (TextView) convertView.findViewById(R.id.hour_value);
        TextView status_value = (TextView) convertView.findViewById(R.id.status_value);
        TextView notes = (TextView) convertView.findViewById(R.id.notes);

        LinearLayout action_update = (LinearLayout) convertView.findViewById(R.id.action_update);
        LinearLayout action_delete = (LinearLayout) convertView.findViewById(R.id.action_delete);

        action_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteReservation(med);
            }
        });
        action_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                com.sahtek.mydoctor.Reservation dialog = new com.sahtek.mydoctor.Reservation(_context, med.getProfile_id());
                dialog.Update = true;
                dialog.reservation = med;
                dialog.fragment = fragment;
                dialog.show();
            }
        });


        date_value.setText(med.getRequested_date());
        hour_value.setText(med.getRequested_time());
        String note = med.getNotes() + "";
        if (!note.equals("null")) {
            notes.setText(note);
        } else {
            notes.setText("");
        }
        if (med.getConfirmed().equals("1")) {
            status_value.setText(_context.getString(R.string.status_conf));
        } else if (med.getRejected().equals("1")) {
            status_value.setText(_context.getString(R.string.status_refus));
        } else {
            status_value.setText(_context.getString(R.string.status_wait));
        }


        return convertView;
    }


    private void DeleteReservation(final Reservation reservation) {
        JSONObject jdata = new JSONObject();
        StringEntity entity;
        String url = AppConstants.DELETE_RESERVATION_URL + "/" + reservation.getId();
        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.postAutorized(_context.getApplicationContext(),
                    url, new SessionManager(_context).getAccessToken(), entity,
                    "application/json", new JsonHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            if (errorResponse != null) {
                                Toast.makeText(_context,
                                        errorResponse.toString(), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(_context,
                                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                        .show();

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                            Toast.makeText(_context,
                                    _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.e("response", "response " + response.toString());
                            String msg = ResponseParser.parseMessageResponse(response);
                            String rsp = ResponseParser.parseResultResponse(response);
                            if (rsp.equals("+OK")) {
                                Toast.makeText(_context,
                                        msg, Toast.LENGTH_SHORT)
                                        .show();
                                DeleteItem(reservation);


                            } else {
                                Toast.makeText(_context,
                                        msg, Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }

                    });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }


}


