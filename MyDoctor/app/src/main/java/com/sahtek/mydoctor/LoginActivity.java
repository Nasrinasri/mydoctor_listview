package com.sahtek.mydoctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;
import com.sahtek.mydoctor.utils.Tools;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

public class LoginActivity extends AppCompatActivity {

    public static String access_token = "";
    TextView forgotpassword, signup_bt, login_bt;
    EditText name_ed, password_ed;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager = new SessionManager(LoginActivity.this);
        InitialiseView();
        AddClickListennner();
        Tools.systemBarLolipop(this);
        String email = sessionManager.getEmail();
        String pass = sessionManager.getPassword();
        name_ed.setText(email);
        password_ed.setText(pass);
        if (!email.isEmpty() && !pass.isEmpty()) {
            login_bt.performClick();
        }
    }

    private void EnableDisable(Boolean enable) {
        forgotpassword.setEnabled(enable);
        signup_bt.setEnabled(enable);
        login_bt.setEnabled(enable);
        name_ed.setEnabled(enable);
        password_ed.setEnabled(enable);
    }

    private void InitialiseView() {
        forgotpassword = (TextView) findViewById(R.id.forgotpassword);
        signup_bt = (TextView) findViewById(R.id.signup_bt);
        login_bt = (TextView) findViewById(R.id.login_bt);

        name_ed = (EditText) findViewById(R.id.name_ed);
        password_ed = (EditText) findViewById(R.id.password_ed);
    }

    private void AddClickListennner() {
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent ii = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });

        signup_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent ii = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });

        login_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /* */
                String email = name_ed.getText().toString().trim();
                String password = password_ed.getText().toString().trim();

                if (AppUtility.isInternetConnected(LoginActivity.this)) {
                    if (TextUtils.isEmpty(email)) {
                        name_ed.setError(getString(R.string.empty_email_error));
                    } else if (!(AppUtility.isValidEmail(email))) {
                        name_ed.setError(getString(R.string.ivalid_email_format));
                    } else if (TextUtils.isEmpty(password)) {
                        name_ed.setError(null);
                        password_ed.setError(getString(R.string.empty_password_error));

                    } else {
                        name_ed.setError(null);
                        password_ed.setError(null);
                        Login(email, password);

                    }

                } else {
                    Toast.makeText(LoginActivity.this,
                            R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                            .show();
                }

            }
        });
    }

    private void Login(final String email, final String password) {

        JSONObject jdata = new JSONObject();
        String Connect_URL = AppConstants.LOGIN_URL;
        try {
            jdata.put(AppConstants.username, email);
            jdata.put(AppConstants.password, password);
        } catch (Exception ex) {
        }
        StringEntity entity;
        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.post(getApplication(), Connect_URL, entity, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    Intent ii = new Intent(LoginActivity.this, ValidateActivity.class);
                    ii.putExtra("email", email);
                    startActivity(ii);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);


                    EnableDisable(true);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {

                    Intent ii = new Intent(LoginActivity.this, ValidateActivity.class);
                    ii.putExtra("email", email);
                    startActivity(ii);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    EnableDisable(true);
                }

                @Override
                public void onStart() {
                    EnableDisable(false);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    Log.e("response", "response " + response.toString());
                    EnableDisable(true);
                    access_token = ResponseParser.parseAccessTokenResponse(response);
                    Intent ii = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(ii);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);

                    sessionManager.saveEmail(email.trim());
                    sessionManager.savePassword(password.trim());
                    sessionManager.saveAccessToken(access_token);
                    finish();
                }

            });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }
}
