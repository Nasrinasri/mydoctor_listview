package com.sahtek.mydoctor;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.adapters.NotificationAdapter;
import com.sahtek.mydoctor.models.Notif;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class NotificationActivity extends AppCompatActivity {

    StickyListHeadersListView list;
    ArrayList<Notif> _listNotification = new ArrayList<Notif>();
    NotificationAdapter adapter;
    ProgressBar pbr;
    String nextPage = "null";
    RelativeLayout list_footer;
    boolean first = true;
    private boolean loadingMore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        setupActionBar();
        IntialiseView();
        if (AppUtility.isInternetConnected(NotificationActivity.this)) {
            GetListNotifs();

        } else {
            Toast.makeText(NotificationActivity.this,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }

    }

    private void IntialiseView() {

        list = (StickyListHeadersListView) findViewById(R.id.list);
        pbr = (ProgressBar) findViewById(R.id.pbr);
        list_footer = (RelativeLayout) findViewById(R.id.list_footer);
        adapter = new NotificationAdapter(NotificationActivity.this, _listNotification);
        list.setAdapter(adapter);

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            // actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setLogo(R.mipmap.ic_bar);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }


    private void GetListNotifs() {
        String Connect_URL = "";
        String prefix = "notifications";
        if (first) {
            Connect_URL = AppRestClient.BASE_URL + prefix;
        } else {
            Connect_URL = nextPage;
        }

        AppRestClient.getFirst(Connect_URL, new SessionManager(this).getAccessToken(), new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(NotificationActivity.this,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(NotificationActivity.this,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(NotificationActivity.this,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onStart() {

                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                } else {
                    list_footer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                //nextPage = ResponseParser.parseMedNextPage(response);
                if (rsp.equals("+OK")) {
                    _listNotification.addAll(ResponseParser.parseListNotifsResponse(response));
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(NotificationActivity.this,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });

    }

}


