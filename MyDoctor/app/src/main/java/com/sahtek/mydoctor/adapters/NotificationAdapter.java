package com.sahtek.mydoctor.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.models.Notif;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by nasri on 30/09/2017.
 */

public class NotificationAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    ArrayList<Notif> listNotification;
    private LayoutInflater inflater;
    Context _context;

    public NotificationAdapter(Context context, ArrayList<Notif> listNotification) {
        inflater = LayoutInflater.from(context);
        this.listNotification = listNotification;
        this._context = context;
    }

    @Override
    public int getCount() {
        return listNotification.size();
    }

    @Override
    public Object getItem(int position) {
        return listNotification.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        NotificationAdapter.ViewHolder holder;

        if (convertView == null) {
            holder = new NotificationAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.notif_layout, parent, false);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.owner = (TextView) convertView.findViewById(R.id.owner);
            holder.open = (TextView) convertView.findViewById(R.id.open);
            holder.adresse = (TextView) convertView.findViewById(R.id.adresse);
            holder.speciality = (TextView) convertView.findViewById(R.id.speciality);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.action_delete = (LinearLayout) convertView.findViewById(R.id.action_delete);
            convertView.setTag(holder);
        } else {
            holder = (NotificationAdapter.ViewHolder) convertView.getTag();
        }


        holder.name.setText(listNotification.get(position).getAppointmentDoctor());

        holder.owner.setVisibility(View.VISIBLE);
        holder.owner.setText(listNotification.get(position).getNotificationType());
        holder.open.setVisibility(View.VISIBLE);
        holder.open.setText(listNotification.get(position).getAppointmentDate() + " " + listNotification.get(position).getAppointmentTime());
        holder.adresse.setVisibility(View.GONE);
        holder.speciality.setVisibility(View.GONE);
        holder.date.setText(listNotification.get(position).getCreated_at());

        holder.action_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteMessage(listNotification.get(position));
            }
        });

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        NotificationAdapter.HeaderViewHolder holder;
        if (convertView == null) {
            holder = new NotificationAdapter.HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header_menu, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (NotificationAdapter.HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        String headerText = "" + listNotification.get(position).getHeader();
        holder.text.setText(headerText);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        // return listEvents.get(position).getStart_date().subSequence(0, 1).charAt(0);
        return listNotification.get(position).getIdheader();
    }

    class HeaderViewHolder {
        TextView text;

    }

    class ViewHolder {
        TextView name;
        TextView owner;
        TextView open;
        TextView adresse;
        TextView speciality;
        TextView date;
        LinearLayout action_delete;
    }


    private void DeleteMessage(final Notif notif) {

        AppRestClient.get(AppConstants.DELETE_NOTIF_URL + notif.getId()
                , new SessionManager(_context).getAccessToken(),
                new JsonHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        if (errorResponse != null) {
                            Toast.makeText(_context,
                                    errorResponse.toString(), Toast.LENGTH_SHORT)
                                    .show();
                        } else {
                            Toast.makeText(_context,
                                    _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                    .show();

                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                        Toast.makeText(_context,
                                _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                .show();
                    }

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.e("response", "response " + response.toString());
                        String message = ResponseParser.parseMessageKeyResponse(response, "msg");
                        String rsp = ResponseParser.parseResultResponse(response);
                        if (rsp.equals("+OK")) {
                            Toast.makeText(_context,
                                    message, Toast.LENGTH_SHORT)
                                    .show();
                            listNotification.remove(notif);
                            notifyDataSetChanged();

                        } else {
                            Toast.makeText(_context,
                                    message, Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }

                });


    }


}
