package com.sahtek.mydoctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.adapters.ListMedAdapter;
import com.sahtek.mydoctor.models.Med;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class PharmacyMedActivity extends AppCompatActivity {

    ListView med_list;
    ListMedAdapter adapter;

    ArrayList<Med> _listMed = new ArrayList<Med>();
    ProgressBar pbr;
    String nextPage = "null";
    private boolean loadingMore = false;
    RelativeLayout list_footer;
    boolean first = true;
    EditText search_ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacy_med);
        setupActionBar();
        IntialiseView();

        if (AppUtility.isInternetConnected(PharmacyMedActivity.this)) {
            GetListMed("");

        } else {
            Toast.makeText(PharmacyMedActivity.this,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }

    }

    private void IntialiseView() {

        med_list = (ListView) findViewById(R.id.med_list);

        pbr = (ProgressBar) findViewById(R.id.pbr);
        list_footer = (RelativeLayout) findViewById(R.id.list_footer);
        search_ed = (EditText) findViewById(R.id.search_ed);

        adapter = new ListMedAdapter(PharmacyMedActivity.this, _listMed);
        med_list.setAdapter(adapter);

        search_ed.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String name = search_ed.getText().toString().trim();
                if(name.isEmpty()){
                    FiltreMed("");
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1,
                                      int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
        });

        search_ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String name = search_ed.getText().toString().trim();
                    FiltreMed(name);
                    return true;
                }
                return false;
            }
        });


        med_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent ii = new Intent(PharmacyMedActivity.this, FindPaharmacyActivity.class);
                Med prof = (Med) adapter.getItem(i);
                ii.putExtra("med_id", prof.getId());
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

    }

    private void FiltreMed(String name) {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        first = true;
        _listMed.clear();
        adapter.notifyDataSetChanged();
        if (AppUtility.isInternetConnected(PharmacyMedActivity.this)) {
            loadingMore = true;
            GetListMed(name);

        } else {
            Toast.makeText(PharmacyMedActivity.this,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }


    private void GetListMed(String name) {
        String Connect_URL = "";
        String prefix = "";
        if (name.isEmpty()) {
            prefix = "medicine";

        } else {
            prefix = "medicine/find?name=" + name;
        }

        if (first) {
            Connect_URL = AppRestClient.BASE_URL + prefix;
        } else {
            Connect_URL = nextPage;
        }


        AppRestClient.getFirst(Connect_URL, new SessionManager(this).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(PharmacyMedActivity.this,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(PharmacyMedActivity.this,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(PharmacyMedActivity.this,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onStart() {

                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                } else {
                    list_footer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                nextPage = ResponseParser.parseMedNextPage(response);
                if (rsp.equals("+OK")) {
                    _listMed.addAll(ResponseParser.parseMedResponse(response));
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(PharmacyMedActivity.this,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            // actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setLogo(R.mipmap.ic_bar);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nmain, menu);
        MenuItem item_message = menu.findItem(R.id.action_message);
        item_message.setActionView(R.layout.messahe_with_count);
        RelativeLayout messageCount = (RelativeLayout) item_message.getActionView();
        TextView messageCountMenuText = messageCount.findViewById(R.id.message_count);
        messageCountMenuText.setText(MainActivity.rsp);
        if(MainActivity.rsp.equals("0")) {
            messageCountMenuText.setVisibility(View.GONE);
        } else {
            messageCountMenuText.setVisibility(View.VISIBLE);
        }


        item_message.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(PharmacyMedActivity.this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                AppUtility.showFilterPopup(findViewById(R.id.action_settings), this);
                return true;

            case R.id.action_message:
                Intent ii = new Intent(this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;

            case R.id.action_notif:
                Intent notif = new Intent(this, NotificationActivity.class);
                startActivity(notif);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
