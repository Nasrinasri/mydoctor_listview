package com.sahtek.mydoctor.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zack on 02/04/2018.
 */

public class Author implements Parcelable {

    public static final Creator<Author> CREATOR = new Creator<Author>() {
        @Override
        public Author createFromParcel(Parcel in) {
            return new Author(in);
        }

        @Override
        public Author[] newArray(int size) {
            return new Author[size];
        }
    };

    private String primaryRole;
    private String idNumber;
    private Integer regionId;
    private String avatar;
    private String lastLogin;
    private Integer id;
    private String updatedAt;
    private Integer publishedArticles;
    private String email;
    private String identification;
    private Integer activated;
    private String name;
    private String createdAt;
    private Integer idTypeId;
    private Integer blocked;
    private String uuid;
    private String mobile;

    public Author() {
    }

    protected Author(Parcel in) {
        primaryRole = in.readString();
        idNumber = in.readString();
        if (in.readByte() == 0) {
            regionId = null;
        } else {
            regionId = in.readInt();
        }
        avatar = in.readString();
        lastLogin = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        updatedAt = in.readString();
        if (in.readByte() == 0) {
            publishedArticles = null;
        } else {
            publishedArticles = in.readInt();
        }
        email = in.readString();
        identification = in.readString();
        if (in.readByte() == 0) {
            activated = null;
        } else {
            activated = in.readInt();
        }
        name = in.readString();
        createdAt = in.readString();
        if (in.readByte() == 0) {
            idTypeId = null;
        } else {
            idTypeId = in.readInt();
        }
        if (in.readByte() == 0) {
            blocked = null;
        } else {
            blocked = in.readInt();
        }
        uuid = in.readString();
        mobile = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(primaryRole);
        dest.writeString(idNumber);
        if (regionId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(regionId);
        }
        dest.writeString(avatar);
        dest.writeString(lastLogin);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(updatedAt);
        if (publishedArticles == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(publishedArticles);
        }
        dest.writeString(email);
        dest.writeString(identification);
        if (activated == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(activated);
        }
        dest.writeString(name);
        dest.writeString(createdAt);
        if (idTypeId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(idTypeId);
        }
        if (blocked == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(blocked);
        }
        dest.writeString(uuid);
        dest.writeString(mobile);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getPrimaryRole() {
        return primaryRole;
    }

    public void setPrimaryRole(String primaryRole) {
        this.primaryRole = primaryRole;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getPublishedArticles() {
        return publishedArticles;
    }

    public void setPublishedArticles(Integer publishedArticles) {
        this.publishedArticles = publishedArticles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public Integer getActivated() {
        return activated;
    }

    public void setActivated(Integer activated) {
        this.activated = activated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getIdTypeId() {
        return idTypeId;
    }

    public void setIdTypeId(Integer idTypeId) {
        this.idTypeId = idTypeId;
    }

    public Integer getBlocked() {
        return blocked;
    }

    public void setBlocked(Integer blocked) {
        this.blocked = blocked;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}