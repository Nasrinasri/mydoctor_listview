package com.sahtek.mydoctor.models;

/**
 * Created by nasri on 20/01/2018.
 */

public class Notif {
    private  String id = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotifiable_id() {
        return notifiable_id;
    }

    public void setNotifiable_id(String notifiable_id) {
        this.notifiable_id = notifiable_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getAppointmentDoctor() {
        return appointmentDoctor;
    }

    public void setAppointmentDoctor(String appointmentDoctor) {
        this.appointmentDoctor = appointmentDoctor;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    private  String notifiable_id = "";
    private  String type = "";
    private  String notificationType = "";

    public Notif(String id, String type, String created_at) {
        this.id = id;
        this.type = type;
        this.created_at = created_at;
    }

    private  String appointmentDate = "";
    private  String appointmentTime = "";
    private  String appointmentDoctor = "";
    private  String created_at = "";
    private  String header = "";

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public long getIdheader() {
        return Idheader;
    }

    public void setIdheader(long idheader) {
        Idheader = idheader;
    }

    private  long Idheader = 0;
}
