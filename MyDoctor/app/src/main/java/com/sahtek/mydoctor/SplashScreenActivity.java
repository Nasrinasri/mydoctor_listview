package com.sahtek.mydoctor;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.sahtek.mydoctor.utils.SessionManager;
import com.sahtek.mydoctor.utils.Tools;

public class SplashScreenActivity extends AppCompatActivity {

    ImageView pb, pb1, pb2;
    SessionManager sessionManager;
    private Animation fadein1, fadein2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sessionManager = new SessionManager(this);
        InitialiseView();
        AnimationConfig();
        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    private void InitialiseView() {
        pb = (ImageView) findViewById(R.id.pb);
        pb1 = (ImageView) findViewById(R.id.pb1);
        pb2 = (ImageView) findViewById(R.id.pb2);

    }

    private void AnimationConfig() {
        fadein1 = AnimationUtils.loadAnimation(SplashScreenActivity.this, R.anim.fadein);
        fadein2 = AnimationUtils.loadAnimation(SplashScreenActivity.this, R.anim.fadein);

        fadein1.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                pb2.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                pb1.startAnimation(fadein2);
            }
        });

        fadein2.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                pb1.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        if (sessionManager.isLoggedIn()) {
                            Intent ii = new Intent(SplashScreenActivity.this, MainActivity.class);
                            startActivity(ii);
                            overridePendingTransition(R.anim.left_in, R.anim.left_out);
                            finish();
                        } else {
                            Intent ii = new Intent(SplashScreenActivity.this, LoginActivity.class);
                            startActivity(ii);
                            overridePendingTransition(R.anim.left_in, R.anim.left_out);
                            finish();
                        }
                    }
                }, 500);
            }
        });

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                {
                    pb2.startAnimation(fadein1);
                }
            }
        }, 500);
    }
}
