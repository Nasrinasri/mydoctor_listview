package com.sahtek.mydoctor.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.adapters.ReservationAdapter;
import com.sahtek.mydoctor.models.Reservation;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ReservationFragment extends Fragment {

    ProgressBar pbr;
    RelativeLayout list_footer;
    ListView list;
    Context _context;
    String category = "";
    ArrayList<Reservation> ListReservation = new ArrayList<Reservation>();
    boolean first = true;
    String nextPage = "null";
    private boolean loadingMore = false;
    ReservationAdapter adapter;

    public ReservationFragment() {
        // Required empty public constructor
    }

    public static ReservationFragment newInstance() {
        ReservationFragment fragment = new ReservationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        _context = getActivity();
        if (AppUtility.isInternetConnected(_context)) {
            GetListReservations();

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reservation, container, false);
        InitialiseView(rootView);
        AddClickListenner();
        return rootView;
    }

    private void InitialiseView(View rootView) {

        list = (ListView) rootView.findViewById(R.id.list);
        pbr = (ProgressBar) rootView.findViewById(R.id.pbr);
        list_footer = (RelativeLayout) rootView.findViewById(R.id.list_footer);

        adapter = new ReservationAdapter(_context, ListReservation, this);
        list.setAdapter(adapter);

    }

    private void AddClickListenner() {

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            }
        });

       /* list.setOnScrollListener( new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int currentFirstVisPos = view.getFirstVisiblePosition();
                if(!nextPage.equals("null") && !nextPage.equals("")) {

                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    if ((lastInScreen == totalItemCount) && !(loadingMore)) {

                        if (AppUtility.isInternetConnected(getActivity())) {
                            loadingMore = true;
                            GetListReservations();

                        }else {

                            Toast.makeText(getActivity(),
                                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                                    .show();
                        }
                    }


                }

            }
        }); */

    }


    public void GetListReservations() {
        String Connect_URL = "";

        if (first) {
            Connect_URL = AppRestClient.BASE_URL + AppConstants.RESERVATIONS_URL;
        } else {
            Connect_URL = AppRestClient.BASE_URL + AppConstants.RESERVATIONS_URL;
        }


        AppRestClient.getFirst(Connect_URL, new SessionManager(getActivity()).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onStart() {

                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                } else {
                    list_footer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                //nextPage = ResponseParser.parsePromotionssNextPage(response);
                nextPage = "";
                if (rsp.equals("+OK")) {
                    ListReservation.clear();
                    ListReservation.addAll(ResponseParser.parseReservationResponse(response));
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }


}