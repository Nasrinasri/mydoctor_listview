package com.sahtek.mydoctor.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.DoctorDetailsActivity;
import com.sahtek.mydoctor.MainActivity;
import com.sahtek.mydoctor.PharmayDetailsActivity;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.ToolsLabDetailActivity;
import com.sahtek.mydoctor.adapters.DoctorAdapter;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class HomeFragment extends Fragment implements View.OnClickListener {

    GridView gridview;
    DoctorAdapter adapter;
    ArrayList<BusinessProfil> _listBusiness = new ArrayList<BusinessProfil>();
    MainActivity _activity;
    boolean first = true;
    LinearLayout toptab_beauty,toptab_glass, toptab_tools, toptab_lab, toptab_phar, toptab_doc;
    Context _context;
    ProgressBar pbr;
    String nextPage = "null";
    private boolean loadingMore = false;
    RelativeLayout list_footer;
    EditText search_ed;
    String name = "";

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        _context = getActivity();


        if (AppUtility.isInternetConnected(_context)) {
            String Connect_URL = AppRestClient.BASE_URL + AppConstants.BUSINESS_URL + "?is_featured=1";
            GetListBusiness(Connect_URL);

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }

    private void FiltreProfiles() {

        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)_context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        first = true;
        _listBusiness.clear();
        adapter.notifyDataSetChanged();
        if (AppUtility.isInternetConnected(_context)) {

            String Connect_URL = AppRestClient.BASE_URL + AppConstants.BUSINESS_URL + "?name=" + name + "&is_featured=1";
            loadingMore = true;
            GetListBusiness(Connect_URL);

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        _activity = (MainActivity) getActivity();
        IntialiseView(rootView);
        AddActionClickLintener();
        return rootView;
    }

    private void IntialiseView(View rootView) {
        gridview = (GridView) rootView.findViewById(R.id.gridview);

        toptab_beauty = (LinearLayout) rootView.findViewById(R.id.toptab_beauty);
        toptab_glass = (LinearLayout) rootView.findViewById(R.id.toptab_glass);
        toptab_tools = (LinearLayout) rootView.findViewById(R.id.toptab_tools);
        toptab_lab = (LinearLayout) rootView.findViewById(R.id.toptab_lab);
        toptab_phar = (LinearLayout) rootView.findViewById(R.id.toptab_phar);
        toptab_doc = (LinearLayout) rootView.findViewById(R.id.toptab_doc);
        pbr = (ProgressBar) rootView.findViewById(R.id.pbr);
        list_footer = (RelativeLayout) rootView.findViewById(R.id.list_footer);
        search_ed = (EditText) rootView.findViewById(R.id.search_ed);
        toptab_beauty.setOnClickListener(this);
        toptab_glass.setOnClickListener(this);
        toptab_tools.setOnClickListener(this);
        toptab_lab.setOnClickListener(this);
        toptab_phar.setOnClickListener(this);
        toptab_doc.setOnClickListener(this);

        adapter = new DoctorAdapter(getActivity(), _listBusiness);
        gridview.setAdapter(adapter);


    }

    private void AddActionClickLintener() {

        search_ed.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                name = search_ed.getText().toString().trim();
                if(name.isEmpty()){
                    FiltreProfiles();
                }


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1,
                                      int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
        });

        search_ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    FiltreProfiles();
                    return true;
                }
                return false;
            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BusinessProfil profil = (BusinessProfil) adapter.getItem(i);
                Intent ii = null;
                if (profil.getType().equals("pharmacy")) {
                    ii = new Intent(getActivity(), PharmayDetailsActivity.class);
                } else if (profil.getType().equals("doctor")) {
                    ii = new Intent(getActivity(), DoctorDetailsActivity.class);
                } else {
                    ii = new Intent(getActivity(), ToolsLabDetailActivity.class);
                }
                ii.putExtra("profile_id", profil.getId());
                ii.putExtra("profile", profil);
                getActivity().startActivity(ii);
                getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        gridview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int currentFirstVisPos = view.getFirstVisiblePosition();
                if (!nextPage.equals("null") && !nextPage.equals("")) {

                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    if ((lastInScreen == totalItemCount) && !(loadingMore)) {

                        if (AppUtility.isInternetConnected(getActivity())) {
                            loadingMore = true;
                            GetListBusiness(nextPage);

                        } else {

                            Toast.makeText(getActivity(),
                                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                                    .show();
                        }
                    }


                }

            }
        });


    }


    @Override
    public void onClick(View view) {

        Fragment fragment = null;
        if (view.getId() == R.id.toptab_doc) {
            fragment = new DoctorFragment();
        }
        if (view.getId() == R.id.toptab_phar) {
            fragment = new PharmacyFragment();
        }
        if (view.getId() == R.id.toptab_lab) {
            fragment = new LaboFragment();
        }
        if (view.getId() == R.id.toptab_tools) {
            fragment = new ToolsFragment();
        }
        if (view.getId() == R.id.toptab_glass) {
            fragment = new GlassesFragment();
        }
        if (view.getId() == R.id.toptab_beauty) {
            fragment = new BeautyFragment();
        }

        if (fragment != null)
            _activity.UpdateView(fragment);

    }


    private void GetListBusiness(String Connect_URL) {


        AppRestClient.getFirst(Connect_URL, new SessionManager(_context).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onStart() {
                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                } else {
                    list_footer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                nextPage = ResponseParser.parseNextPage(response);
                if (rsp.equals("+OK")) {
                    _listBusiness.addAll(ResponseParser.parseBusinessProfilesResponse(response));
                    adapter.notifyDataSetChanged();


                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }
}
