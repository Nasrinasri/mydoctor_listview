package com.sahtek.mydoctor;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ValidateActivity extends AppCompatActivity {

    TextView cancel_bt, send_bt;
    EditText code_ed, email_ed, phone_ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate);
        setupActionBar();
        InitialiseView();
        AddClickListenner();

        Bundle extras = getIntent().getExtras();
        String email = extras.getString("email");
        email_ed.setText(email);
        SessionManager session = new SessionManager(ValidateActivity.this);
        phone_ed.setText(session.getPhoneNumber(email));
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void InitialiseView() {

        cancel_bt = (TextView) findViewById(R.id.cancel_bt);
        send_bt = (TextView) findViewById(R.id.send_bt);
        code_ed = (EditText) findViewById(R.id.code_ed);
        email_ed = (EditText) findViewById(R.id.name_ed);
        phone_ed = (EditText) findViewById(R.id.phone_ed);
    }

    private void EnableDisable(Boolean enable) {
        cancel_bt.setEnabled(enable);
        send_bt.setEnabled(enable);
        code_ed.setEnabled(enable);
        email_ed.setEnabled(enable);
        phone_ed.setEnabled(enable);
    }

    private void AddClickListenner() {

        cancel_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        send_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = code_ed.getText().toString().trim();
                String email = email_ed.getText().toString().trim();
                String phone = phone_ed.getText().toString().trim();

                if (AppUtility.isInternetConnected(ValidateActivity.this)) {

                    if (TextUtils.isEmpty(code)) {
                        code_ed.setError(getString(R.string.empty_code_error));

                    } else if (code.length() != 9) {
                        code_ed.setError(getString(R.string.empty_code_error2));

                    } else if (TextUtils.isEmpty(email)) {
                        code_ed.setError(null);
                        email_ed.setError(getString(R.string.empty_email_error));
                    } else if (!(AppUtility.isValidEmail(email))) {
                        code_ed.setError(null);
                        email_ed.setError(getString(R.string.ivalid_email_format));
                    } else if (phone.length() < 10 || !AppUtility.isValidPhoneNumber(phone)) {
                        code_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(getString(R.string.empty_phone_error));
                    } else {
                        code_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(null);
                        Validate(code, email, phone);
                    }

                } else {
                    Toast.makeText(ValidateActivity.this,
                            R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                            .show();
                }


            }
        });
    }


    private void Validate(String code, String email, String phone) {

        JSONObject jdata = new JSONObject();
        String Connect_URL = AppConstants.ACTIVATE_URL;
        try {
            jdata.put(AppConstants.mobile, phone);
            jdata.put(AppConstants.email, email);
            jdata.put(AppConstants.token, code);
        } catch (Exception ex) {
        }
        StringEntity entity;

        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.post(getApplication(), Connect_URL, entity, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    if (errorResponse != null) {
                        Log.e("errorResponse", "errorResponse " + errorResponse.toString());
                        Toast.makeText(getApplicationContext(),
                                errorResponse.toString(), Toast.LENGTH_SHORT)
                                .show();

                    } else {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.connexion_problem), Toast.LENGTH_SHORT)
                                .show();

                    }


                    EnableDisable(true);
                }

                @Override
                public void onStart() {
                    EnableDisable(false);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    Log.e("response", "response " + response.toString());
                    EnableDisable(true);
                    String rsp = ResponseParser.parseResultResponse(response);
                    if (rsp.equals("+OK")) {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.compte_activee), Toast.LENGTH_LONG)
                                .show();
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.connexion_problem), Toast.LENGTH_LONG)
                                .show();

                    }


                }

            });

        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }
}
