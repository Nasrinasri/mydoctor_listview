package com.sahtek.mydoctor.models;

/**
 * Created by nasri on 13/01/2018.
 */

public class Comment {
    private String id = "";
    private String comment = "";
    private String updated_at = "";
    private String authorName = "";
    private String avatar = "";

    public Comment(String id, String comment, String updated_at) {
        this.id = id;
        this.comment = comment;
        this.updated_at = updated_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
