package com.sahtek.mydoctor.models;

/**
 * Created by nasri on 23/09/2017.
 */

public class Doctor {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSepliciality() {
        return sepliciality;
    }

    public void setSepliciality(String sepliciality) {
        this.sepliciality = sepliciality;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    private String id;

    public Doctor(String id, String firstName, String lastName, String sepliciality, int rank) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sepliciality = sepliciality;
        this.rank = rank;
    }

    private String firstName;
    private String lastName;
    private String sepliciality;
    private int rank;

}
