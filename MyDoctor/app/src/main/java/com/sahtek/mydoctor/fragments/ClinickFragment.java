package com.sahtek.mydoctor.fragments;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sahtek.mydoctor.R;

import java.util.ArrayList;
import java.util.List;


public class ClinickFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TextView tabOne,tabtwo;
    public ClinickFragment() {
        // Required empty public constructor
    }

    public static ClinickFragment newInstance() {
        ClinickFragment fragment = new ClinickFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_clinick, container, false);
        InitialiseView(rootview);
        return rootview;
    }

    private void InitialiseView( View rootview)
    {
        viewPager = (ViewPager)  rootview.findViewById(R.id.viewpager);
        tabLayout = (TabLayout)  rootview.findViewById(R.id.tabs);

        TabLayout tabLayout = (TabLayout) rootview.findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));
        tabLayout.setSelectedTabIndicatorHeight(7);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        viewPager.setCurrentItem(1);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });


    }


    private void setupTabIcons() {

        LinearLayout layout1 = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabOne =  (TextView) layout1.findViewById(R.id.tab);
        tabOne.setText(R.string.fav);
        tabLayout.getTabAt(1).setCustomView(tabOne);

        LinearLayout layout2 = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabtwo =  (TextView) layout2.findViewById(R.id.tab);
        tabtwo.setText(R.string.reserv);
        tabLayout.getTabAt(0).setCustomView(tabtwo);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(ReservationFragment.newInstance(), "");
        adapter.addFrag(MyCliniqueFragment.newInstance(), "");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }




}
