package com.sahtek.mydoctor;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.sahtek.mydoctor.utils.AppUtility.loadPhoto;


public class DoctorDetailsActivity extends AppCompatActivity {

    String profile_id = "";
    BusinessProfil doctor = null;
    TextView name, sepliciality, description, datetime, phone, mobile, adresse, price;
    LinearLayout topbottomcall, topaction_message, topaction_review, topaction_reservation, topaction_favoris;
    MapView mMapView;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_details);
        setupActionBar();
        InitialiseMapView(savedInstanceState);
        name = (TextView) findViewById(R.id.name);
        sepliciality = (TextView) findViewById(R.id.sepliciality);
        description = (TextView) findViewById(R.id.description);
        price = (TextView) findViewById(R.id.price);
        adresse = (TextView) findViewById(R.id.adresse);
        mobile = (TextView) findViewById(R.id.mobile);
        phone = (TextView) findViewById(R.id.phone);
        datetime = (TextView) findViewById(R.id.datetime);

        topbottomcall = (LinearLayout) findViewById(R.id.topbottomcall);
        topaction_message = (LinearLayout) findViewById(R.id.topaction_message);
        topaction_review = (LinearLayout) findViewById(R.id.topaction_review);
        topaction_reservation = (LinearLayout) findViewById(R.id.topaction_reservation);
        topaction_favoris = (LinearLayout) findViewById(R.id.topaction_favoris);

        AddClickListenner();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            profile_id = extras.getString("profile_id");
        }
        Intent mIntent = getIntent();
        doctor = (BusinessProfil) mIntent.getParcelableExtra("profile");
        if (doctor != null) {
            name.setText(doctor.getName());
            datetime.setText(doctor.getOpening_days() + " " + doctor.getOpening_time() + " -> " + doctor.getClosing_time());
            sepliciality.setText(doctor.getSpeciality_details());
            description.setText(doctor.getDescription());
            phone.setText(doctor.getPhone() + " ");
            mobile.setText(doctor.getMobile() + " ");
            adresse.setText(doctor.getAddress() + " ");
            price.setText(doctor.getConsultation_fee() + " "+getString(R.string.currency));
            ImageView img = findViewById(R.id.img);
            if(doctor.getUser() != null)
            {loadPhoto(this, doctor.getUser().getAvatar(), img);}
        }

        if (AppUtility.isInternetConnected(DoctorDetailsActivity.this)) {
            // GetDoctorDetail();

        } else {
            Toast.makeText(DoctorDetailsActivity.this,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }


    }

    private String speciality(String id) {
        String speciality = "";
        for (int i = 0; i < MainActivity.ListCategory.size(); i++) {
            if (MainActivity.ListCategory.get(i).getId().equals(id)) {
                return MainActivity.ListCategory.get(i).getName();
            }
        }
        return speciality;
    }

    private void AddClickListenner() {

        topbottomcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtility.makeCall(DoctorDetailsActivity.this, doctor.getPhone());

            }
        });

        topaction_favoris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtility.AddFavoris(DoctorDetailsActivity.this, doctor.getId());
            }
        });

        topaction_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMessage dialog = new SendMessage(DoctorDetailsActivity.this, doctor.getUser_id());
                dialog.show();
            }
        });

        topaction_reservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Reservation dialog = new Reservation(DoctorDetailsActivity.this, doctor.getUser_id());
                dialog.show();
            }
        });


        topaction_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateProfile dialog = new RateProfile(DoctorDetailsActivity.this, doctor.getId());
                dialog.show();
            }
        });

    }


    //MapView configuration

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            // actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setLogo(R.mipmap.ic_bar);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    private void InitialiseMapView(Bundle savedInstanceState) {
        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button

                BitmapDescriptor iconPayer = BitmapDescriptorFactory.fromResource(R.drawable.payer_mark);
                LatLng doctorLocation = new LatLng(Float.parseFloat(doctor.getLatitude()), Float.parseFloat(doctor.getLongitude()));
                googleMap.addMarker(new MarkerOptions().position(doctorLocation).title(doctor.getName()).snippet(doctor.getType()));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(doctorLocation).zoom(14).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if (ContextCompat.checkSelfPermission(DoctorDetailsActivity.this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    googleMap.setMyLocationEnabled(true);


                    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    if (myLocation == null) {
                        Criteria criteria = new Criteria();
                        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                        String provider = lm.getBestProvider(criteria, true);
                        myLocation = lm.getLastKnownLocation(provider);
                    }

                    if (myLocation != null) {
                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.part_marker);
                        LatLng sydney1 = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        //.icon(icon)
                        googleMap.addMarker(new MarkerOptions().position(sydney1).title("My position").snippet("---"));
                    }

                } else {
                    ActivityCompat.requestPermissions(DoctorDetailsActivity.this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            700);
                }


            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 700: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onResume();

                } else {
                    Log.i("Permission", "Permission denied by user.");
                }
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nmain, menu);
        MenuItem item_message = menu.findItem(R.id.action_message);
        item_message.setActionView(R.layout.messahe_with_count);
        RelativeLayout messageCount = (RelativeLayout) item_message.getActionView();
        TextView messageCountMenuText = messageCount.findViewById(R.id.message_count);
        messageCountMenuText.setText(MainActivity.rsp);
        if(MainActivity.rsp.equals("0")) {
            messageCountMenuText.setVisibility(View.GONE);
        } else {
            messageCountMenuText.setVisibility(View.VISIBLE);
        }


        item_message.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(DoctorDetailsActivity.this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                AppUtility.showFilterPopup(findViewById(R.id.action_settings), this);
                return true;

            case R.id.action_message:
                Intent ii = new Intent(this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;

            case R.id.action_notif:
                Intent notif = new Intent(this, NotificationActivity.class);
                startActivity(notif);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void GetDoctorDetail() {

        String Connect_URL = AppConstants.DETAIL_FEATURED_URL + profile_id;

        AppRestClient.get(Connect_URL, new SessionManager(this).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(DoctorDetailsActivity.this,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(DoctorDetailsActivity.this,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                // pbr.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(DoctorDetailsActivity.this,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                //  pbr.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onStart() {

                //  pbr.setVisibility(View.VISIBLE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //  pbr.setVisibility(View.INVISIBLE);
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {


                } else {
                    Toast.makeText(DoctorDetailsActivity.this,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }

}
