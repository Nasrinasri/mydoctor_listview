package com.sahtek.mydoctor.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.MessageDetailsActivity;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.models.MessageItem;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by nasri .
 */

public class MessageAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    ArrayList<MessageItem> listMessageItem;
    Context _context;
    boolean recived = true;
    private LayoutInflater inflater;

    public MessageAdapter(Context context, ArrayList<MessageItem> listMessageItem, boolean recived) {
        inflater = LayoutInflater.from(context);
        this.listMessageItem = listMessageItem;
        this._context = context;
        this.recived = recived;
    }

    @Override
    public int getCount() {
        return listMessageItem.size();
    }

    @Override
    public Object getItem(int position) {
        return listMessageItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.message_item, parent, false);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.subject = (TextView) convertView.findViewById(R.id.subject);
            holder.message = (TextView) convertView.findViewById(R.id.message);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.img = (CircleImageView) convertView.findViewById(R.id.img);
            holder.layout_mess = (LinearLayout) convertView.findViewById(R.id.layout_mess);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (recived) {
            holder.name.setText(listMessageItem.get(position).getSender_name());
            String avatar = listMessageItem.get(position).getSender_avatar();
            if (avatar.contains("http")) {
                Glide.with(_context).load(avatar).into(holder.img);
            }
            if(listMessageItem.get(position).getRead().equals("1")) {
                holder.layout_mess.setBackgroundColor(_context.getResources().getColor(android.R.color.white));
            }else {
                holder.layout_mess.setBackgroundColor(_context.getResources().getColor(R.color.topdivider));
            }
        } else {
            holder.layout_mess.setBackgroundColor(_context.getResources().getColor(android.R.color.white));
            holder.name.setText(listMessageItem.get(position).getRecipient_name());
            String avatar = listMessageItem.get(position).getRecipient_avatar();
            if (avatar.contains("http")) {
                Glide.with(_context).load(avatar).into(holder.img);
            }
            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    confirmDeleteDialog(listMessageItem.get(position));
                    return true;
                }
            });


        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ii = new Intent(_context, MessageDetailsActivity.class);
                MessageItem mess =  listMessageItem.get(position);
                ii.putExtra("mess", mess);
                ii.putExtra("recived", recived);
                _context.startActivity(ii);
            }
        });

        holder.message.setText(listMessageItem.get(position).getBody());
        holder.message.setVisibility(View.GONE);
        holder.subject.setText(listMessageItem.get(position).getSubject());
        holder.date.setText(AppUtility.getHour(listMessageItem.get(position).getCreated_at()));


        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header_menu, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        String headerText = "" + listMessageItem.get(position).getHeader();
        holder.text.setText(headerText);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        // return listEvents.get(position).getStart_date().subSequence(0, 1).charAt(0);
        //return listMessageItem.get(position).getIdheader();
        return AppUtility.dateMillisecondes(listMessageItem.get(position).getCreated_at());
    }

    private void confirmDeleteDialog(final MessageItem messageItem) {
        View titleView = LayoutInflater.from(_context).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText("حذف الرساله");

        AlertDialog dialog = new AlertDialog.Builder(_context)
                .setCustomTitle(titleView)
                .setMessage("هل انت متأكد من حذف الرساله؟")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        DeleteMessage(messageItem);
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();

    }

    private void DeleteMessage(final MessageItem comment) {

        AppRestClient.get(AppConstants.DELETE_MESSAGE_URL + comment.getUuid()
                , new SessionManager(_context).getAccessToken(),
                new JsonHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        if (errorResponse != null) {
                            Toast.makeText(_context,
                                    errorResponse.toString(), Toast.LENGTH_SHORT)
                                    .show();
                        } else {
                            Toast.makeText(_context,
                                    _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                    .show();

                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                        Toast.makeText(_context,
                                _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                .show();
                    }

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.e("response", "response " + response.toString());
                        String message = ResponseParser.parseMessageKeyResponse(response, "message");
                        String msg = ResponseParser.parseMessageResponse(response);
                        if (msg.equals("+OK")) {
                            Toast.makeText(_context,
                                    message, Toast.LENGTH_SHORT)
                                    .show();
                            listMessageItem.remove(comment);
                            notifyDataSetChanged();

                        } else {
                            Toast.makeText(_context,
                                    msg, Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }

                });


    }

    class HeaderViewHolder {
        TextView text;

    }

    class ViewHolder {
        TextView name;
        TextView message, subject, date;
        CircleImageView img;
        LinearLayout layout_mess;
    }


}