package com.sahtek.mydoctor.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.LoginActivity;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.UpdateProfilActivity;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.ParseException;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by nasri on 30/09/2017.
 */

public class AppUtility {

    public static void onContactClick(Context _context) {

        String object = _context.getString(R.string.app_name);
        String ContactMail = "support@sahtek.com";
        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{ContactMail});
        // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, object);
        emailIntent.setType("message/rfc822");

        PackageManager pm = _context.getPackageManager();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");


        Intent openInChooser = Intent.createChooser(emailIntent, _context.getString(R.string.setting32));

        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if (packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if (packageName.contains("android.gm")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{ContactMail});
                intent.putExtra(Intent.EXTRA_SUBJECT, object);
                intent.setType("message/rfc822");


                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }


        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        _context.startActivity(openInChooser);
    }


    public static void onShareClick(Context _context) {


        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);

        // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
        emailIntent.putExtra(Intent.EXTRA_TEXT, _context.getString(R.string.sendmessage));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, _context.getString(R.string.app_name));
        emailIntent.setType("message/rfc822");

        PackageManager pm = _context.getPackageManager();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");


        Intent openInChooser = Intent.createChooser(emailIntent, _context.getString(R.string.sharenow));

        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if (packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if (packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("mms") || packageName.contains("android.gm") || packageName.contains("com.whatsapp") || packageName.contains("com.whatsapp.ContactPicker")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, _context.getString(R.string.sendmessage));
                if (packageName.contains("android.gm")) {
                    intent.putExtra(Intent.EXTRA_SUBJECT, _context.getString(R.string.app_name));
                    intent.setType("message/rfc822");
                }

                if (packageName.contains("twitter")) {
                    intent.putExtra(Intent.EXTRA_TEXT, _context.getString(R.string.sendmessage));
                }


                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }

        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);


        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);

        _context.startActivity(openInChooser);
    }


    public static void OpenTimeDialog(Context _context) {

        final CharSequence[] day_radio = {_context.getString(R.string.jour), _context.getString(R.string.nuit)};

        View titleView = LayoutInflater.from(_context).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(_context.getString(R.string.open_title));

        AlertDialog.Builder builder2 = new AlertDialog.Builder(_context)
                .setCustomTitle(titleView)
                .setSingleChoiceItems(day_radio, -1, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        AlertDialog alertdialog2 = builder2.create();
        alertdialog2.show();

    }

    public static void showFilterPopup(View v, final Activity _context) {
        PopupMenu popup = new PopupMenu(_context, v, Gravity.CENTER);

        popup.getMenuInflater().inflate(R.menu.menu_main, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_help:
                        Intent profil = new Intent(_context, UpdateProfilActivity.class);
                        _context.startActivity(profil);
                        _context.overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        return true;
                    case R.id.action_record_number:
                        View titleView = LayoutInflater.from(_context).inflate(R.layout.dialog_custom_title, null);
                        TextView title = titleView.findViewById(R.id.tv_dialog_title);
                        title.setText(_context.getString(R.string.text_record_number));
                        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();
                        alertDialog.setCustomTitle(titleView);
                        alertDialog.setMessage(new SessionManager(_context).getUserId());
                        alertDialog.setCancelable(true);
                        alertDialog.show();
                        TextView messageView = alertDialog.findViewById(android.R.id.message);
                        messageView.setGravity(Gravity.CENTER);

                        return true;
                    case R.id.action_logout:
                        Intent about = new Intent(_context.getApplicationContext(), LoginActivity.class);
                        about.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        _context.startActivity(about);
                        _context.overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        SessionManager sessionManager = new SessionManager(_context);
                        sessionManager.logoutUser();
                        _context.finishAffinity();
                        return true;

                    case R.id.action_review:
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + _context.getPackageName()));
                        _context.startActivity(browserIntent);
                        return true;

                    case R.id.action_contactus:
                        AppUtility.onContactClick(_context);
                        return true;
                    case R.id.action_share:
                        AppUtility.onShareClick(_context);
                        return true;

                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    // Internet is connected or not
    public static boolean isInternetConnected(Context mContext) {

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static boolean isValidEmail(CharSequence target) {

        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    public static void makeCall(Context _context, String phone) {

        if (ContextCompat.checkSelfPermission(_context,
                android.Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + phone));
            _context.startActivity(intent);


        } else {
            ActivityCompat.requestPermissions((Activity) _context,
                    new String[]{android.Manifest.permission.CALL_PHONE},
                    100);
        }


    }

    public static void AddFavoris(final Context _context, String id) {

        JSONObject jdata = new JSONObject();
        String Connect_URL = AppConstants.ADD_FAVORIS_URL;
        try {
            jdata.put(AppConstants.profile_id, id);
        } catch (Exception ex) {
        }
        StringEntity entity;

        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.postAutorized(_context, Connect_URL, new SessionManager(_context).getAccessToken(), entity, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (errorResponse != null) {
                        Toast.makeText(_context,
                                errorResponse.toString(), Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(_context,
                                _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                .show();

                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }

                @Override
                public void onStart() {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.e("response", "response " + response.toString());
                    String rsp = ResponseParser.parseResultResponse(response);
                    String msg = ResponseParser.parseMessageResponse(response);
                    if (rsp.equals("+OK")) {
                        Toast.makeText(_context,
                                msg, Toast.LENGTH_SHORT)
                                .show();

                    } else {
                        Toast.makeText(_context,
                                msg, Toast.LENGTH_SHORT)
                                .show();
                    }
                }

            });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }

    public static Float parseFloat(String input) {
        Float result = Float.valueOf(0);
        try {
            result = Float.parseFloat(input);
        } catch (Exception e) {
        }

        return result;
    }


    public static String headerTitle(String date) {
        String title = getDay(date);
        return title;
    }

    public static String getDay(String date) {
        Calendar cal = Calendar.getInstance();
        long value = cal.getTimeInMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            cal.setTimeInMillis(timeInMilliseconds);
            value = cal.getTimeInMillis();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        cal.setTimeInMillis(value);
        String retour = formatter.format(cal.getTime());
        return retour;
    }

    public static String getHour(String date) {
        Calendar cal = Calendar.getInstance();
        long value = cal.getTimeInMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            cal.setTimeInMillis(timeInMilliseconds);
            value = cal.getTimeInMillis();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        cal.setTimeInMillis(value);
        String retour = formatter.format(cal.getTime());
        return retour;
    }

    public static long dateMillisecondes(String date) {
        Calendar cal = Calendar.getInstance();
        long value = cal.getTimeInMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            cal.setTimeInMillis(timeInMilliseconds);

            cal.set(Calendar.HOUR_OF_DAY, 00); //set hour to last hour
            cal.set(Calendar.MINUTE, 0); //set minutes to last minute
            cal.set(Calendar.SECOND, 0); //set seconds to last second
            cal.set(Calendar.MILLISECOND, 0); //set milliseconds to last millisecond
            value = cal.getTimeInMillis();

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return value;
    }

    public static Calendar getCalendarFromTime(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        try {
            Date date = sdf.parse(time);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date.getTime());
            return calendar;
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isDay(String time) {
        Calendar c = getCalendarFromTime(time);
        if (c == null) {
            return true;
        }
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 6 && timeOfDay < 18) {
            return true;
        } else if (timeOfDay >= 18 && timeOfDay < 6) {
            return false;
        }
        return true;
    }

    public static void loadPhoto(Context _context, String url, ImageView img) {
        if (url == null) {
            return;
        }
        if (!url.startsWith("http")) {
            url = AppConstants.IMAGE_PATH + url;
        }
        RequestOptions requestOptions = RequestOptions.centerCropTransform()
                .transform(new CircleCrop()).placeholder(R.drawable.thumb);
        Glide.with(_context)
                .load(url)
                .apply(requestOptions)
                .into(img);
    }

    public static String getRealPathFromURIAPI11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;

    }

    public static String getRealPathFromURIAPI19(Context context, Uri contentUri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(contentUri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }


    public  static  String getType (Context _context, String category) {
       String result = _context.getString(R.string.doctor) ;
       if(category.equals("pharmacy")) {
           result = _context.getString(R.string.pharmacy) ;
       }
       if(category.equals("laboratory")) {
           result = _context.getString(R.string.labos) ;
       }
       if(category.equals("medical-supplier")) {
           result = _context.getString(R.string.tools) ;
       }
       if(category.equals("optrician")) {
           result = _context.getString(R.string.eyewear) ;
       }
       if(category.equals("beauty")) {
           result = _context.getString(R.string.beauty) ;
       }
       return  result;

    }
}
