package com.sahtek.mydoctor;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sahtek.mydoctor.models.Region;
import com.sahtek.mydoctor.models.Type;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;
import com.sahtek.mydoctor.utils.Tools;
import com.sahtek.mydoctor.utils.Utility;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpActivity extends AppCompatActivity {

    EditText name_ed, age_ed, email_ed, phone_ed, password_ed, password_ed_re, type_number;
    TextView send_bt, clear_bt, type_txt, region_txt;
    CircleImageView pick_picture;
    ArrayList<Region> ListRegion = new ArrayList<Region>();
    ArrayList<Type> ListType = new ArrayList<Type>();
    String region_id = "0";
    String type_id = "0";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private File file = null;
    private RadioButton radio0, radio1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Tools.systemBarLolipop(this);
        InitialiseView();
        AddClickListenner();

        if (AppUtility.isInternetConnected(SignUpActivity.this)) {
            GetListRegion();
            GetListType();

        } else {

            Toast.makeText(SignUpActivity.this,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();
            finish();

        }
    }


    private void InitialiseView() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        name_ed = (EditText) findViewById(R.id.name_ed);
        age_ed = (EditText) findViewById(R.id.age_ed);
        email_ed = (EditText) findViewById(R.id.email_ed);
        phone_ed = (EditText) findViewById(R.id.phone_ed);
        type_number = (EditText) findViewById(R.id.type_number);

        password_ed = (EditText) findViewById(R.id.password_ed);
        password_ed_re = (EditText) findViewById(R.id.password_ed_re);

        send_bt = (TextView) findViewById(R.id.send_bt);
        clear_bt = (TextView) findViewById(R.id.clear_bt);
        type_txt = (TextView) findViewById(R.id.type_txt);
        region_txt = (TextView) findViewById(R.id.region_txt);

        pick_picture = (CircleImageView) findViewById(R.id.pick_picture);

        radio0 = (RadioButton) findViewById(R.id.radio0);
        radio1 = (RadioButton) findViewById(R.id.radio1);
    }

    private void AddClickListenner() {

        pick_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();

            }
        });

        send_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = name_ed.getText().toString().trim();
                String age = age_ed.getText().toString().trim();
                String email = email_ed.getText().toString().trim();
                String phone = phone_ed.getText().toString().trim();
                String password = password_ed.getText().toString().trim();
                String confir_password = password_ed_re.getText().toString().trim();
                String number = type_number.getText().toString().trim();
                String type = type_txt.getText().toString().trim();
                String region = region_txt.getText().toString().trim();

                if (AppUtility.isInternetConnected(SignUpActivity.this)) {
                    if (TextUtils.isEmpty(name)) {
                        name_ed.setError(getString(R.string.empty_name_error));

                    }
                   /* else if (name.length()<5) {
                        name_ed.setError(getString(R.string.short_name_error));

                    }*/
                    else if (TextUtils.isEmpty(age)) {
                        name_ed.setError(null);
                        age_ed.setError(getString(R.string.empty_age_error));
                    } else if (TextUtils.isEmpty(email)) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(getString(R.string.empty_email_error));
                    } else if (!(AppUtility.isValidEmail(email))) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(getString(R.string.ivalid_email_format));
                    } else if (phone.length() < 10 || !AppUtility.isValidPhoneNumber(phone)) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(getString(R.string.empty_phone_error));
                    } else if (TextUtils.isEmpty(password)) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(null);
                        password_ed.setError(getString(R.string.empty_password_error));
                    } else if (TextUtils.isEmpty(confir_password)) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(null);
                        password_ed.setError(null);
                        password_ed_re.setError(getString(R.string.empty_password_error));
                    } else if (!(password.equals(confir_password))) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(null);
                        password_ed.setText("");
                        password_ed_re.setText("");
                        password_ed.setError(getString(R.string.password_not_match_error));
                        password_ed_re.setError(getString(R.string.password_not_match_error));
                    } else if (region.equals(getString(R.string.region_txt))) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(null);
                        password_ed.setError(null);
                        password_ed_re.setError(null);

                        Toast.makeText(SignUpActivity.this,
                                R.string.region_empty, Toast.LENGTH_SHORT)
                                .show();

                    }
                    /*else if (type.equals(getString(R.string.type_txt))) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(null);
                        password_ed.setError(null);
                        password_ed_re.setError(null);

                        Toast.makeText(SignUpActivity.this,
                                R.string.type_txt_empty, Toast.LENGTH_SHORT)
                                .show();

                    } else if (TextUtils.isEmpty(number)) {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(null);
                        password_ed.setError(null);
                        password_ed_re.setError(null);
                        type_number.setError(getString(R.string.type_number_empty));


                    } */
                    else {
                        name_ed.setError(null);
                        age_ed.setError(null);
                        email_ed.setError(null);
                        phone_ed.setError(null);
                        password_ed.setError(null);
                        password_ed_re.setError(null);
                        type_number.setError(null);

                        if (AppUtility.isInternetConnected(SignUpActivity.this)) {

                            RegisterRequest(name, age, email, phone, password, number);


                        } else {

                            Toast.makeText(SignUpActivity.this,
                                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                                    .show();

                        }


                    }

                } else {

                    Toast.makeText(SignUpActivity.this,
                            R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                            .show();

                }

            }
        });

        clear_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearData();

            }
        });

        region_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegionsDialog();
            }
        });

        type_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TypesDialog();
            }
        });

    }

    private void clearData() {
        name_ed.setText("");
        age_ed.setText("");
        email_ed.setText("");
        phone_ed.setText("");
        password_ed.setText("");
        password_ed_re.setText("");
    }

    private void EnableDisable(boolean enable) {

        name_ed.setEnabled(enable);
        age_ed.setEnabled(enable);
        email_ed.setEnabled(enable);
        phone_ed.setEnabled(enable);
        password_ed.setEnabled(enable);
        password_ed_re.setEnabled(enable);
        clear_bt.setEnabled(enable);
        send_bt.setEnabled(enable);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void RegisterRequest(String name, String age, final String email, final String phone, String password, String number) {
        String Connect_URL = AppConstants.REGISTER_URL;

        RequestParams params = new RequestParams();
        String gender = "1";
        if (radio0.isChecked()) {
            gender = "0";
        }
        try {
            if (file != null) {
                params.put(AppConstants.avatar, file);
            }
            params.put(AppConstants.name, name);
            params.put(AppConstants.email, email);
            params.put(AppConstants.mobile, phone);
            params.put(AppConstants.age, age);
            params.put(AppConstants.gender, gender);
            params.put(AppConstants.password, password);
            params.put(AppConstants.confirm_pass, password);
            params.put(AppConstants.region_id, region_id);
            params.put(AppConstants.id_type_id, type_id);
            params.put(AppConstants.id_number, number);


        } catch (Exception ex) {
        }

        {
            AppRestClient.post(getApplication(), Connect_URL, params, new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {


                    if (errorResponse != null) {
                        Log.e("errorResponse", "errorResponse " + errorResponse.toString());

                        String message = ResponseParser.parseMessageErrResponse(errorResponse);
                        if (message.equals("Non White-listed Destination - rejected")) {
                            clearData();
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.comptecree), Toast.LENGTH_LONG)
                                    .show();
                            SavePhone(email, phone);
                            Intent ii = new Intent(SignUpActivity.this, ValidateActivity.class);
                            ii.putExtra("email", email);
                            startActivity(ii);
                            overridePendingTransition(R.anim.left_in, R.anim.left_out);
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(),
                                    errorResponse.toString(), Toast.LENGTH_SHORT)
                                    .show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.connexion_problem), Toast.LENGTH_SHORT)
                                .show();

                    }


                    EnableDisable(true);
                }

                @Override
                public void onStart() {
                    EnableDisable(false);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    Log.e("response", "response " + response.toString());
                    String rsp = ResponseParser.parseResultResponse(response);
                    String msg = ResponseParser.parseMessageResponse(response);
                    EnableDisable(true);
                    clearData();
                    if (rsp.equals("+OK")) {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.comptecree), Toast.LENGTH_LONG)
                                .show();

                        SavePhone(email, phone);

                        Intent ii = new Intent(SignUpActivity.this, ValidateActivity.class);
                        ii.putExtra("email", email);
                        startActivity(ii);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(),
                                msg, Toast.LENGTH_SHORT)
                                .show();
                    }

                }

            });

        }
    }

    // get region & type

    private void GetListRegion() {

        String Connect_URL = AppConstants.Region_URL;


        AppRestClient.get(Connect_URL, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {

                    ListRegion.addAll(ResponseParser.parseRegionResponse(response));

                } else {
                    Toast.makeText(SignUpActivity.this,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }

    private void GetListType() {

        String Connect_URL = AppConstants.Type_URL;


        AppRestClient.get(Connect_URL, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {

                    ListType.addAll(ResponseParser.parseTypeResponse(response));

                } else {
                    Toast.makeText(SignUpActivity.this,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }

    public void RegionsDialog() {

        final String[] items = new String[ListRegion.size()];

        for (int i = 0; i < items.length; i++) {
            items[i] = ListRegion.get(i).getName();
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        View titleView = LayoutInflater.from(SignUpActivity.this).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.region_txt));

        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                //received_country_value.setText(items[item]);
                region_id = ListRegion.get(item).getId();
                region_txt.setText(ListRegion.get(item).getName());
            }
        }).show();

    }

    public void TypesDialog() {

        final String[] items = new String[ListType.size()];

        for (int i = 0; i < items.length; i++) {
            items[i] = ListType.get(i).getName();
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        View titleView = LayoutInflater.from(SignUpActivity.this).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.type_txt));
        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                //received_country_value.setText(items[item]);
                type_id = ListType.get(item).getId();
                type_txt.setText(ListType.get(item).getName());
            }
        }).show();

    }


    private void SavePhone(String email, String phone) {
        SessionManager session = new SessionManager(SignUpActivity.this);
        session.savePhoneNumber(email, phone);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"التقاط صورة", "اختيار من المكتبة",
                getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        View titleView = LayoutInflater.from(SignUpActivity.this).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText("أضف صورة !");

        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(SignUpActivity.this);

                if (item == 0) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (item == 1) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (item == 3) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        Log.e("destination", "destination " + destination.getAbsolutePath());

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        pick_picture.setImageBitmap(thumbnail);
        file = destination;
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Uri selectedImageUri = data.getData();
        String filestring = getRealPathFromURI(SignUpActivity.this, selectedImageUri);


        pick_picture.setImageBitmap(bm);
        Log.e("filestring", "filestring " + filestring);
        File destination = new File(filestring);
        file = destination;
    }


    public String getRealPathFromURI(Context context, Uri contentUri) {
        String realPath = null;
        if (Build.VERSION.SDK_INT < 19) {
            realPath = AppUtility.getRealPathFromURIAPI11to18(this, contentUri);
        } else {
            realPath = AppUtility.getRealPathFromURIAPI19(this, contentUri);
        }
        return realPath;
    }


}
