package com.sahtek.mydoctor;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.models.MessageItem;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.sahtek.mydoctor.utils.AppUtility.loadPhoto;

public class MessageDetailsActivity extends AppCompatActivity {

    ImageView img;
    TextView name,date,subject,message;
    LinearLayout topaction_message;
    MessageItem _message ;
    boolean recived = false ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_details);
        img = (ImageView) findViewById(R.id.img);
        name = (TextView) findViewById(R.id.name);
        date = (TextView) findViewById(R.id.date);
        subject = (TextView) findViewById(R.id.subject);
        message = (TextView) findViewById(R.id.message);

        topaction_message = (LinearLayout) findViewById(R.id.topaction_message);
        setupActionBar();
        AddClickListenner ();

        Intent mIntent = getIntent();
        _message = (MessageItem) mIntent.getParcelableExtra("mess");
        recived =  mIntent.getBooleanExtra("recived",false);
        if (_message != null) {

            message.setText(_message.getBody());
            subject.setText(_message.getSubject());
           date.setText(_message.getCreated_at());

           String prof_id = "";

            if (recived) {
                name.setText(_message.getSender_name());
                prof_id = _message.getSender_id();
                String avatar = _message.getSender_avatar();
                if (avatar.contains("http")) {
                    Glide.with(MessageDetailsActivity.this).load(avatar).into(img);
                }
            } else {
               name.setText(_message.getRecipient_name());
                prof_id = _message.getReceiver_id();
                String avatar = _message.getRecipient_avatar();
                if (avatar.contains("http")) {
                    Glide.with(MessageDetailsActivity.this).load(avatar).into(img);
                }
                }

            final String finalProf_id = prof_id;
            topaction_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SendMessage dialog = new SendMessage(MessageDetailsActivity.this, finalProf_id);
                    dialog.show();
                }
            });

            if(recived) {
                ReadMessage(_message.getId());
            }

        }


    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            // actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setLogo(R.mipmap.ic_bar);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

    }

    private void AddClickListenner () {

    }


    private void ReadMessage(String message_id) {

        String Connect_URL = "https://www.sahtek.com/web/messages/api/change/state/" + message_id;

        AppRestClient.externGet(Connect_URL, new SessionManager(MessageDetailsActivity.this).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(MessageDetailsActivity.this,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(MessageDetailsActivity.this,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(MessageDetailsActivity.this,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Log.e("response", "messageState " + response.toString());
            }

        });

    }


}
