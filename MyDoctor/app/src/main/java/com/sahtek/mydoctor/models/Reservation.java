package com.sahtek.mydoctor.models;

/**
 * Created by nasri on 13/01/2018.
 */

public class Reservation {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRequested_date() {
        return requested_date;
    }

    public void setRequested_date(String requested_date) {
        this.requested_date = requested_date;
    }

    public String getRequested_time() {
        return requested_time;
    }

    public void setRequested_time(String requested_time) {
        this.requested_time = requested_time;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(String confirmed) {
        this.confirmed = confirmed;
    }

    public String getRejected() {
        return rejected;
    }

    public void setRejected(String rejected) {
        this.rejected = rejected;
    }

    private String id =  "";
    private String profile_id =  "";
    private String user_id =  "";

    public Reservation(String id, String profile_id, String user_id, String requested_date, String requested_time, String notes, String confirmed, String rejected) {
        this.id = id;
        this.profile_id = profile_id;
        this.user_id = user_id;
        this.requested_date = requested_date;
        this.requested_time = requested_time;
        this.notes = notes;
        this.confirmed = confirmed;
        this.rejected = rejected;
    }

    private String requested_date =  "";
    private String requested_time =  "";
    private String notes =  "";
    private String confirmed =  "";
    private String rejected =  "";
}
