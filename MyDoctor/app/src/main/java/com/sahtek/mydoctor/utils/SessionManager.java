package com.sahtek.mydoctor.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {

    private static final String KEY_EMAIL = "key_email";
    private static final String KEY_PHONE_NUMBER = "key_phone_number";
    private static final String KEY_PASSWORD = "key_password";
    private static final String KEY_ACCESS_TOKEN = "key_access_token";
    private static final String KEY_USER_ID = "key_user_id";

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences("MyDoctor_v100",
                PRIVATE_MODE);
        editor = pref.edit();
        //editor.commit();
    }

    public void savePhoneNumber(String email, String phone) {

        editor.putString(email, phone);

        editor.commit();
    }

    public String getPhoneNumber(String email) {
        String path = "";
        path = pref.getString(KEY_PHONE_NUMBER, "12345678910");
        return path;
    }

    public String getEmail() {
        String path = "";
        path = pref.getString(KEY_EMAIL, "");
        return path;
    }

    public String getPassword() {
        String path = "";
        path = pref.getString(KEY_PASSWORD, "");
        return path;
    }

    public String getAccessToken() {
        String path = "";
        path = pref.getString(KEY_ACCESS_TOKEN, "");
        return path;
    }

    public void saveEmail(String email) {
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    public void savePassword(String password) {
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }

    public String getUserId() {
        String path = "";
        path = pref.getString(KEY_USER_ID, "");
        return path;
    }

    public void setUserId(String userId) {
        editor.putString(KEY_USER_ID, userId);
        editor.commit();
    }

    public void savePhoneNumber(String phoneNumber) {
        editor.putString(KEY_PHONE_NUMBER, phoneNumber);
        editor.commit();
    }

    public void saveAccessToken(String accessToken) {
        editor.putString(KEY_ACCESS_TOKEN, accessToken);
        editor.commit();
    }

    public String getData(String id) {
        String path = "";
        path = pref.getString(id, "");
        return path;
    }

    public boolean isLoggedIn() {
        return !getEmail().isEmpty() && !getPassword().isEmpty() && !getAccessToken().isEmpty();
    }

    public void logoutUser() {
        saveEmail("");
        savePassword("");
        saveAccessToken("");
        setUserId("");
    }
}
