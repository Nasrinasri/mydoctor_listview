package com.sahtek.mydoctor.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nasri.
 */

public class MessageItem implements Parcelable {


    protected MessageItem(Parcel in) {
        header = in.readString();
        id = in.readString();
        uuid = in.readString();
        sender_id = in.readString();
        receiver_id = in.readString();
        subject = in.readString();
        body = in.readString();
        read = in.readString();
        anonymous = in.readString();
        created_at = in.readString();
        sender_name = in.readString();
        sender_avatar = in.readString();
        recipient_name = in.readString();
        recipient_avatar = in.readString();
    }

    public static final Creator<MessageItem> CREATOR = new Creator<MessageItem>() {
        @Override
        public MessageItem createFromParcel(Parcel in) {
            return new MessageItem(in);
        }

        @Override
        public MessageItem[] newArray(int size) {
            return new MessageItem[size];
        }
    };

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getSender_avatar() {
        return sender_avatar;
    }

    public void setSender_avatar(String sender_avatar) {
        this.sender_avatar = sender_avatar;
    }

    public String getRecipient_name() {
        return recipient_name;
    }

    public void setRecipient_name(String recipient_name) {
        this.recipient_name = recipient_name;
    }

    public String getRecipient_avatar() {
        return recipient_avatar;
    }

    public void setRecipient_avatar(String recipient_avatar) {
        this.recipient_avatar = recipient_avatar;
    }

    public MessageItem( String id, String uuid, String sender_id, String receiver_id, String subject, String body, String read, String anonymous, String created_at, String sender_name, String sender_avatar, String recipient_name, String recipient_avatar) {
        this.id = id;
        this.uuid = uuid;
        this.sender_id = sender_id;

        this.receiver_id = receiver_id;
        this.subject = subject;
        this.body = body;
        this.read = read;
        this.anonymous = anonymous;
        this.created_at = created_at;
        this.sender_name = sender_name;
        this.sender_avatar = sender_avatar;
        this.recipient_name = recipient_name;
        this.recipient_avatar = recipient_avatar;
    }


    String header;
    String id ="";
    String uuid ="";
    String sender_id ="";
    String receiver_id ="";
    String subject ="";
    String body ="";
    String read ="";
    String anonymous ="";
    String created_at ="";
    String sender_name ="";
    String sender_avatar ="";
    String recipient_name ="";
    String recipient_avatar ="";

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(header);
        parcel.writeString(id);
        parcel.writeString(uuid);
        parcel.writeString(sender_id);
        parcel.writeString(receiver_id);
        parcel.writeString(subject);
        parcel.writeString(body);
        parcel.writeString(read);
        parcel.writeString(anonymous);
        parcel.writeString(created_at);
        parcel.writeString(sender_name);
        parcel.writeString(sender_avatar);
        parcel.writeString(recipient_name);
        parcel.writeString(recipient_avatar);
    }
}
