package com.sahtek.mydoctor;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.models.Comment;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ArticleComment extends Dialog implements
        View.OnClickListener {

    public Button cancel_bt, send_bt;
    public Boolean Update = false;
    public Comment comment = null;
    String article_id = "";
    ProgressBar progressBar1;
    private Context _context;
    private EditText message, subject;
    private CheckBox checkBox;
    private ArticleActivity activity;

    public ArticleComment(Context context, String receiver_id) {
        super(context);
        _context = context;
        this.article_id = receiver_id;
        this.activity = (ArticleActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.send_message_layout);
        setCanceledOnTouchOutside(false);
        initilize();

        initilizeEvent();

    }


    private void initilize() {
        message = (EditText) findViewById(R.id.message);
        subject = (EditText) findViewById(R.id.subject);

        cancel_bt = (Button) findViewById(R.id.cancel_bt);
        send_bt = (Button) findViewById(R.id.send_bt);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

        checkBox = (CheckBox) findViewById(R.id.checkBox);
        TextView text_view_message = (TextView) findViewById(R.id.text_view_message);

        subject.setVisibility(View.GONE);
        checkBox.setVisibility(View.GONE);
        message.setHint(_context.getString(R.string.add_comment_hint));
        text_view_message.setHint(_context.getString(R.string.add_comment_title));

        if (Update && comment != null) {
            message.setText(comment.getComment());
        }

    }


    private void initilizeEvent() {
        cancel_bt.setOnClickListener(this);
        send_bt.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_bt:
                dismiss();

                break;
            case R.id.send_bt:

                if (AppUtility.isInternetConnected(_context)) {

                    String text = message.getText().toString().trim();
                    if (TextUtils.isEmpty(text)) {
                        Toast.makeText(_context,
                                R.string.add_comment_empty, Toast.LENGTH_LONG)
                                .show();

                    } else {
                        if (Update) {
                            Update(text);

                        } else {
                            Send(text);
                        }


                    }

                } else {
                    Toast.makeText(_context,
                            R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                            .show();

                }

                break;
            default:
                break;
        }

    }

    private void Send(final String text) {
        JSONObject jdata = new JSONObject();
        try {
            jdata.put(AppConstants.article_id, article_id);
            jdata.put(AppConstants.comment, text);

        } catch (Exception ex) {
        }
        StringEntity entity;
        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.postAutorized(_context.getApplicationContext(),
                    AppConstants.COMMENT_ARTICLE_URL, new SessionManager(_context).getAccessToken(), entity,
                    "application/json", new JsonHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            if (errorResponse != null) {
                                Toast.makeText(_context,
                                        errorResponse.toString(), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(_context,
                                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                        .show();

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            Toast.makeText(_context,
                                    _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onStart() {

                            progressBar1.setVisibility(View.VISIBLE);
                            EnableDisable(false);

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.e("response", "response " + response.toString());
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            String rsp = ResponseParser.parseResultResponse(response);
                            String msg = ResponseParser.parseMessageResponse(response);
                            if (rsp.equals("+OK")) {
                                Toast.makeText(_context,
                                        msg, Toast.LENGTH_SHORT)
                                        .show();
                                dismiss();
                                activity.GetComment(article_id);


                            } else {
                                Toast.makeText(_context,
                                        msg, Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }

                    });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }


    private void Update(final String text) {
        JSONObject jdata = new JSONObject();
        try {
            jdata.put(AppConstants.comment_id, comment.getId());
            jdata.put(AppConstants.comment, text);

        } catch (Exception ex) {
        }
        StringEntity entity;

        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.postAutorized(_context.getApplicationContext(),
                    AppConstants.EDIT_COMMENT_ARTICLE_URL, new SessionManager(_context).getAccessToken(), entity,
                    "application/json", new JsonHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            if (errorResponse != null) {
                                Toast.makeText(_context,
                                        errorResponse.toString(), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(_context,
                                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                        .show();

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            Toast.makeText(_context,
                                    _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onStart() {

                            progressBar1.setVisibility(View.VISIBLE);
                            EnableDisable(false);

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.e("response", "response " + response.toString());
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            String rsp = ResponseParser.parseResultResponse(response);
                            String msg = ResponseParser.parseMessageResponse(response);
                            String message = ResponseParser.parseMessageKeyResponse(response, "message");
                            if (msg.equals("+OK")) {
                                Toast.makeText(_context,
                                        message, Toast.LENGTH_SHORT)
                                        .show();
                                dismiss();
                                activity.GetComment(article_id);


                            } else {
                                Toast.makeText(_context,
                                        msg, Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }

                    });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }

    private void EnableDisable(boolean enable) {
        cancel_bt.setEnabled(enable);
        send_bt.setEnabled(enable);
        message.setEnabled(enable);
        subject.setEnabled(enable);
    }


}

