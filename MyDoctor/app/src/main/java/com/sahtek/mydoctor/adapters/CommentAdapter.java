package com.sahtek.mydoctor.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.ArticleActivity;
import com.sahtek.mydoctor.ArticleComment;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.models.Comment;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by nasri on 13/01/2018.
 */

public class CommentAdapter extends BaseAdapter {
    Context _context;
    ArrayList<Comment> _listItems = new ArrayList<Comment>();
    String articleId = "";


    public CommentAdapter(Context context, ArrayList<Comment> _listItems, String articleId) {
        this._context = context;
        this._listItems = _listItems;
        this.articleId = articleId;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this._listItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this._listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Comment med = (Comment) getItem(position);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = infalInflater.inflate(R.layout.comment_item, null);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView sepliciality = (TextView) convertView.findViewById(R.id.sepliciality);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        ImageView image = (ImageView) convertView.findViewById(R.id.img);
        name.setSelected(true);

        LinearLayout action_update = (LinearLayout) convertView.findViewById(R.id.action_update);
        LinearLayout action_delete = (LinearLayout) convertView.findViewById(R.id.action_delete);

        action_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteComment(med);
            }
        });
        action_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArticleComment dialog = new ArticleComment(_context, articleId);
                dialog.Update = true;
                dialog.comment = med;
                dialog.show();
            }
        });


        name.setText(med.getAuthorName());
        sepliciality.setText(med.getComment());
        date.setText(med.getUpdated_at());
        AppUtility.loadPhoto(_context, med.getAvatar(), image);
        return convertView;
    }


    private void DeleteComment(final Comment comment) {
        JSONObject jdata = new JSONObject();
        try {
            jdata.put(AppConstants.comment_id, comment.getId());

        } catch (Exception ex) {
        }
        StringEntity entity;

        try {

            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.postAutorized(_context.getApplicationContext(),
                    AppConstants.DELETE_COMMENT_ARTICLE_URL, new SessionManager(_context).getAccessToken(), entity,
                    "application/json", new JsonHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            if (errorResponse != null) {
                                Toast.makeText(_context,
                                        errorResponse.toString(), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(_context,
                                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                        .show();

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                            Toast.makeText(_context,
                                    _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.e("response", "response " + response.toString());
                            String message = ResponseParser.parseMessageKeyResponse(response, "message");
                            String msg = ResponseParser.parseMessageResponse(response);
                            if (msg.equals("+OK")) {
                                Toast.makeText(_context,
                                        message, Toast.LENGTH_SHORT)
                                        .show();
                                ArticleActivity activity = (ArticleActivity) _context;
                                activity.ListComment.remove(comment);
                                activity.adapter.notifyDataSetChanged();


                            } else {
                                Toast.makeText(_context,
                                        msg, Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }

                    });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }


}

