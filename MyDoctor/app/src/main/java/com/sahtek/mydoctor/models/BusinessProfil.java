package com.sahtek.mydoctor.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nasri on 07/12/2017.
 */

public class BusinessProfil implements Parcelable {


    private String id = "";
    private String user_id = "";
    private String description = "";
    private String favorisId = "";
    private String region_id = "";
    private String category_id = "";
    private String type = "";
    private String phone = "";
    private String fax = "";
    private String mobile = "";
    private String address = "";
    private String opening_days = "";
    private String opening_time = "";
    private String closing_time = "";
    private String latitude = "0";
    private String longitude = "0";
    private String stars = "";
    private String uuid = "";
    private String gender = "";
    private String viewable = "";
    private String consultation_fee = "";
    private String is_featured = "";
    private String owner = "";
    private String name;

    protected BusinessProfil(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        description = in.readString();
        favorisId = in.readString();
        region_id = in.readString();
        category_id = in.readString();
        type = in.readString();
        phone = in.readString();
        fax = in.readString();
        mobile = in.readString();
        address = in.readString();
        opening_days = in.readString();
        opening_time = in.readString();
        closing_time = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        stars = in.readString();
        uuid = in.readString();
        gender = in.readString();
        viewable = in.readString();
        consultation_fee = in.readString();
        is_featured = in.readString();
        owner = in.readString();
        name = in.readString();
        speciality_details = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(description);
        dest.writeString(favorisId);
        dest.writeString(region_id);
        dest.writeString(category_id);
        dest.writeString(type);
        dest.writeString(phone);
        dest.writeString(fax);
        dest.writeString(mobile);
        dest.writeString(address);
        dest.writeString(opening_days);
        dest.writeString(opening_time);
        dest.writeString(closing_time);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(stars);
        dest.writeString(uuid);
        dest.writeString(gender);
        dest.writeString(viewable);
        dest.writeString(consultation_fee);
        dest.writeString(is_featured);
        dest.writeString(owner);
        dest.writeString(name);
        dest.writeString(speciality_details);
        dest.writeParcelable(user, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusinessProfil> CREATOR = new Creator<BusinessProfil>() {
        @Override
        public BusinessProfil createFromParcel(Parcel in) {
            return new BusinessProfil(in);
        }

        @Override
        public BusinessProfil[] newArray(int size) {
            return new BusinessProfil[size];
        }
    };

    public String getSpeciality_details() {
        return speciality_details;
    }

    public void setSpeciality_details(String speciality_details) {
        this.speciality_details = speciality_details;
    }

    private String speciality_details= "";
    private User user;


    public BusinessProfil(String id, String region_id, String type, String name) {
        this.id = id;
        this.region_id = region_id;
        this.type = type;
        this.name = name;
    }

    public BusinessProfil(String id, String user_id, String region_id, String category_id, String type, String phone, String fax, String mobile, String address, String opening_days, String opening_time, String closing_time, String latitude, String longitude, String stars) {
        this.id = id;
        this.user_id = user_id;
        this.region_id = region_id;
        this.category_id = category_id;
        this.type = type;
        this.phone = phone;
        this.fax = fax;
        this.mobile = mobile;
        this.address = address;
        this.opening_days = opening_days;
        this.opening_time = opening_time;
        this.closing_time = closing_time;
        this.latitude = latitude;
        this.longitude = longitude;
        this.stars = stars;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFavorisId() {
        return favorisId;
    }

    public void setFavorisId(String favorisId) {
        this.favorisId = favorisId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOpening_days() {
        return opening_days;
    }

    public void setOpening_days(String opening_days) {
        this.opening_days = opening_days;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getViewable() {
        return viewable;
    }

    public void setViewable(String viewable) {
        this.viewable = viewable;
    }

    public String getConsultation_fee() {
        return consultation_fee;
    }

    public void setConsultation_fee(String consultation_fee) {
        this.consultation_fee = consultation_fee;
    }

    public String getIs_featured() {
        return is_featured;
    }

    public void setIs_featured(String is_featured) {
        this.is_featured = is_featured;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User avatar) {
        this.user = avatar;
    }

}
