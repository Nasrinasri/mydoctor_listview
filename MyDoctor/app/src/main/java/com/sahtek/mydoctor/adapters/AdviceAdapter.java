package com.sahtek.mydoctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.models.Article;

import java.util.ArrayList;

import static com.sahtek.mydoctor.utils.AppUtility.loadPhoto;

/**
 * Created by nasri on 11/01/2018.
 */

public class AdviceAdapter extends BaseAdapter {
    Context _context;
    ArrayList<Article> _listItems = new ArrayList<Article>();


    public AdviceAdapter(Context context, ArrayList<Article> _listItems) {
        this._context = context;
        this._listItems = _listItems;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this._listItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this._listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Article artcile = (Article) getItem(position);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = infalInflater.inflate(R.layout.article_item, null);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView sepliciality = (TextView) convertView.findViewById(R.id.sepliciality);
        TextView adress = (TextView) convertView.findViewById(R.id.adress);

        ImageView img = (ImageView) convertView.findViewById(R.id.img);
        loadPhoto(_context, artcile.getAuthor().getAvatar(), img);

        name.setText(artcile.getAuthor().getName());
        sepliciality.setText(artcile.getTitle());
        adress.setText(artcile.getCreated_at());

        return convertView;
    }


}
