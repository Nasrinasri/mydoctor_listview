package com.sahtek.mydoctor.models;

/**
 * Created by nasri on 28/09/2017.
 */

public class LaboToolsEyeswear {

    public LaboToolsEyeswear(String id, String name, String opentime, String adresse, String owner) {
        this.id = id;
        this.name = name;
        this.opentime = opentime;
        this.adresse = adresse;
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    private String id;
    private String name;
    private String opentime;
    private String adresse;
    private String owner;
}
