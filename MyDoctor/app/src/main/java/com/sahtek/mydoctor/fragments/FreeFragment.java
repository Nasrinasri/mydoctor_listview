package com.sahtek.mydoctor.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.DoctorDetailsActivity;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.adapters.PromotionsAdapter;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.models.Promotions;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class FreeFragment extends Fragment {

    TextView search_ed;
    ProgressBar pbr;
    RelativeLayout list_footer;
    ListView list;
    Context _context;
    String category = "";
    ArrayList<Promotions> ListPromotions = new ArrayList<Promotions>();
    boolean first = true;
    String nextPage = "null";
    private boolean loadingMore = false;
    PromotionsAdapter adapter;
    private static ProgressDialog _progress;

    public FreeFragment() {
        // Required empty public constructor
    }

    public static FreeFragment newInstance() {
        FreeFragment fragment = new FreeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        _context = getActivity();
        if (AppUtility.isInternetConnected(_context)) {
            GetListFree();

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_free, container, false);
        InitialiseView(rootView);
        AddClickListenner();
        return rootView;
    }

    private void InitialiseView(View rootView) {

        list = (ListView) rootView.findViewById(R.id.list);
        pbr = (ProgressBar) rootView.findViewById(R.id.pbr);
        search_ed = (TextView) rootView.findViewById(R.id.search_ed);
        list_footer = (RelativeLayout) rootView.findViewById(R.id.list_footer);

        adapter = new PromotionsAdapter(_context, ListPromotions);
        list.setAdapter(adapter);

    }

    private void AddClickListenner() {
        search_ed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CategoriesDialog();
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Promotions free = (Promotions) adapterView.getItemAtPosition(i);
                GetDoctorDetail(free.getProfile_id());
            }
        });

       /* list.setOnScrollListener( new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int currentFirstVisPos = view.getFirstVisiblePosition();
                if(!nextPage.equals("null") && !nextPage.equals("")) {

                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    if ((lastInScreen == totalItemCount) && !(loadingMore)) {

                        if (AppUtility.isInternetConnected(getActivity())) {
                            loadingMore = true;
                            GetListFree();

                        }else {

                            Toast.makeText(getActivity(),
                                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                                    .show();
                        }
                    }


                }

            }
        }); */

    }

    private void FiltreFree() {
        first = true;
        ListPromotions.clear();
        adapter.notifyDataSetChanged();
        if (AppUtility.isInternetConnected(_context)) {
            GetListFree();

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }

    private void GetListFree() {
        String Connect_URL = "";

        if (first) {
            Connect_URL = AppRestClient.BASE_URL + AppConstants.PROMOTIONS_URL;
            Connect_URL = Connect_URL + "" + category;
        } else {
            Connect_URL = nextPage;
        }


        AppRestClient.getFirst(Connect_URL, new SessionManager(getActivity()).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onStart() {

                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                } else {
                    list_footer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
                Log.e("response free", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                //nextPage = ResponseParser.parsePromotionssNextPage(response);
                nextPage = "";
                if (rsp.equals("+OK")) {

                    try {
                        JSONArray data = response.getJSONArray(AppConstants.data);
                        ListPromotions.addAll(ResponseParser.parsePromotionsResponse(data));
                      /*  if (data.has(AppConstants.doctor)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.doctor);
                            ListPromotions.addAll(ResponseParser.parsePromotionsResponse(profiles));
                        }
                        if (data.has(AppConstants.pharmacy)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.pharmacy);
                            ListPromotions.addAll(ResponseParser.parsePromotionsResponse(profiles));
                        }

                        if (data.has(AppConstants.laboratory)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.laboratory);
                            ListPromotions.addAll(ResponseParser.parsePromotionsResponse(profiles));
                        }

                        if (data.has(AppConstants.medicalsupplier)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.medicalsupplier);
                            ListPromotions.addAll(ResponseParser.parsePromotionsResponse(profiles));
                        }

                        if (data.has(AppConstants.optrician)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.optrician);
                            ListPromotions.addAll(ResponseParser.parsePromotionsResponse(profiles));
                        } */

                        adapter.notifyDataSetChanged();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }


    public void CategoriesDialog() {

        final String[] items = new String[7];
        items[0] = _context.getString(R.string.all);
        items[1] = _context.getString(R.string.doctors);
        items[2] = _context.getString(R.string.pharmacy);
        items[3] = _context.getString(R.string.labos);
        items[4] = _context.getString(R.string.tools);
        items[5] = _context.getString(R.string.eyewear);
        items[6] = _context.getString(R.string.beauty);

        final ArrayList<String> categorys = new ArrayList<String>();
        categorys.add("");
        categorys.add("?category=doctor");
        categorys.add("?category=pharmacy");
        categorys.add("?category=laboratory");
        categorys.add("?category=medical-supplier");
        categorys.add("?category=optrician");
        categorys.add("?category=beauty");

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View titleView = LayoutInflater.from(_context).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.action_category));

        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (!category.equals(categorys.get(item))) {
                    category = categorys.get(item);
                    search_ed.setText(items[item]);
                    FiltreFree();
                }

            }
        }).show();

    }


    private void displayDialog() {
        _progress = new ProgressDialog(_context);
        _progress.setMessage(_context.getString(R.string.waitmessage));
        _progress.show();
        _progress.setCanceledOnTouchOutside(false);
        _progress.setCancelable(false);
    }

    private void hideDialog() {

        _progress.dismiss();
    }

    private void GetDoctorDetail(String profile_id) {

        String Connect_URL = AppConstants.DETAIL_FEATURED_URL + profile_id;


        AppRestClient.get(Connect_URL, new SessionManager(_context).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                hideDialog();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                hideDialog();
            }

            @Override
            public void onStart() {

                displayDialog();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                hideDialog();
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {

                    BusinessProfil prof = ResponseParser.parseSingleBusinessProfilesResponse(response);
                     if(prof != null) {
                         Intent ii = new Intent(_context, DoctorDetailsActivity.class);
                         ii.putExtra("profile_id", prof.getId());
                         ii.putExtra("profile", prof);
                         getActivity().startActivity(ii);
                         getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
                     }

                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }
}