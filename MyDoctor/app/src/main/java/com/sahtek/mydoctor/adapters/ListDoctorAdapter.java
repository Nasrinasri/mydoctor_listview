package com.sahtek.mydoctor.adapters;

/**
 * Created by nasri on 27/09/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sahtek.mydoctor.MainActivity;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.RateProfile;
import com.sahtek.mydoctor.Reservation;
import com.sahtek.mydoctor.SendMessage;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.utils.AppUtility;

import java.util.ArrayList;

import static com.sahtek.mydoctor.utils.AppUtility.loadPhoto;


public class ListDoctorAdapter extends BaseAdapter {
    Context _context;
    ArrayList<BusinessProfil> _listItems = new ArrayList<BusinessProfil>();


    public ListDoctorAdapter(Context context, ArrayList<BusinessProfil> _listItems) {
        this._context = context;
        this._listItems = _listItems;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this._listItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this._listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final BusinessProfil doctor = (BusinessProfil) getItem(position);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = infalInflater.inflate(R.layout.doctor_listveiew_item, null);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView sepliciality = (TextView) convertView.findViewById(R.id.sepliciality);
        TextView adress = (TextView) convertView.findViewById(R.id.adress);
        TextView sep = (TextView) convertView.findViewById(R.id.sep);

        LinearLayout action_call = (LinearLayout) convertView.findViewById(R.id.action_call);
        LinearLayout action_message = (LinearLayout) convertView.findViewById(R.id.action_message);
        LinearLayout action_review = (LinearLayout) convertView.findViewById(R.id.action_review);
        LinearLayout action_reserve = (LinearLayout) convertView.findViewById(R.id.action_reserve);
        LinearLayout action_favoris = (LinearLayout) convertView.findViewById(R.id.action_favoris);

        ImageView img = (ImageView) convertView.findViewById(R.id.img);
        loadPhoto(_context, doctor.getUser().getAvatar(), img);

        action_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtility.makeCall(_context, doctor.getPhone());

            }
        });

        action_favoris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtility.AddFavoris(_context, doctor.getId());
            }
        });

        action_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMessage dialog = new SendMessage(_context, doctor.getUser_id());
                dialog.show();
            }
        });

        action_reserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Reservation dialog = new Reservation(_context, doctor.getUser_id());
                dialog.show();
            }
        });


        action_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateProfile dialog = new RateProfile(_context, doctor.getId());
                dialog.show();
            }
        });

        name.setText(doctor.getName());
        sepliciality.setText(doctor.getOpening_time() + " -> " + doctor.getClosing_time());
        adress.setText(doctor.getAddress());
        //sep.setText(speciality(doctor.getCategory_id()));
        sep.setText(doctor.getSpeciality_details());

        return convertView;
    }

    private String speciality(String id) {
        String speciality = "";
        for (int i = 0; i < MainActivity.ListCategory.size(); i++) {
            if (MainActivity.ListCategory.get(i).getId().equals(id)) {
                return MainActivity.ListCategory.get(i).getName();
            }
        }
        return speciality;
    }


}
