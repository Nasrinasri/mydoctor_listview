package com.sahtek.mydoctor.utils;


public class AppConstants {
    public static final int DEFAULT_TIMEOUT = 10 * 1000;

    public static final String SUCCESS_CONSTANT = "true";
    public static final String FAILURE_CONSTANT = "false";
    public static final long DISCONNECT_TIMEOUT = 60000;

    public static int RESULT_LOAD_IMG = 1;
    public static int RESULT_PICK_IMG = 2;


    //Webservices methods
    public static final String LOGIN_URL = "login";
    public static final String REGISTER_URL = "register";
    public static final String SAVE_PROFIL_URL = "profile/save";
    public static final String GET_PROFIL_URL = "profile";
    public static final String ACTIVATE_URL = "activate";
    public static final String FORGOT_URL = "forgot-password";
    public static final String RESET_URL = "reset-password";
    public static final String LOGOUT_URL = "user/userlogout";
    public static final String FEATURED_URL = "business-profiles";
    public static final String BUSINESS_URL = "business-profiles";
    public static final String ARTICLE_URL = "articles/list";
    public static final String Region_URL = "regions";
    public static final String Category_URL = "categories/doctor";
    public static final String ADVICE_Category_URL = "articles/categories";
    public static final String Type_URL = "id-types";
    public static final String DETAIL_FEATURED_URL = "business-info/";
    public static final String ADD_FAVORIS_URL = "myclinic/add";
    public static final String REMOVE_FAVORIS_URL = "myclinic/delete";
    public static final String GET_FAVORIS_URL = "myclinic";
    public static final String SEND_MESSAGE_URL = "send-short-message";
    public static final String RESERVATION_URL = "make-reservation";
    public static final String EDIT_RESERVATION_URL = "edit-reservation";
    public static final String RATE_URL = "business-rate";
    public static final String ARTICLE_RATE_URL = "articles/rate";
    public static final String READ_ARTICLE_URL = "articles/read";
    public static final String COMMENT_ARTICLE_URL = "articles/comment";
    public static final String GET_COMMENT_ARTICLE_URL = "articles/my-comments";
    public static final String DELETE_COMMENT_ARTICLE_URL = "articles/delete-my-comment";
    public static final String DELETE_RESERVATION_URL = "cancel-reservation";
    public static final String EDIT_COMMENT_ARTICLE_URL = "articles/edit-my-comment";
    public static final String PROMOTIONS_URL = "promotions";
    public static final String RESERVATIONS_URL = "my-reservations";
    public static final String MESSAGE_URL = "message-inbox";
    public static final String DELETE_MESSAGE_URL = "message-delete/";
    public static final String DELETE_NOTIF_URL = "notification/delete/";


    public static final String GET_PLACES_URL = "places";
    public static final String GET_EVENT_URL = "/events";
    public static final String GET_place_INFO_URL = "/getInfos";

    public static final String GET_TICKETS_URL = "yurplan/tickets/";
    public static final String GET_TICKETS_DETAILS_URL = "tickets/";

    public static final String UPDATE_PICTURE_URL = "user/%s/updatePicture";


    public static final String URL_GET_QR_IMAGE = "";
    public static final String URL_RADIO_DETAILS = "";
    public static final String URL_RADIO = "";


    //Keys tags

    public static final String success = "success";
    public static final String rsp = "rsp";
    public static final String newmessage = "newmessage";
    public static final String msg = "msg";
    public static final String message = "message";
    public static final String username = "username";
    public static final String name = "name";
    public static final String avatar = "avatar";
    public static final String description = "description";
    public static final String medicines = "medicines";
    public static final String owner = "owner";
    public static final String email = "email";
    public static final String token = "token";
    public static final String password_confirmation = "password_confirmation";
    public static final String mobile = "mobile";
    public static final String password = "password";
    public static final String confirm_pass = "confirm_pass";
    public static final String Authorization = "Authorization";
    public static final String access_token = "access_token";
    public static final String age = "age";

    public static final String profiles = "profiles";
    public static final String profile = "profile";
    public static final String data = "data";
    public static final String doctor = "doctor";
    public static final String pharmacy = "pharmacy";
    public static final String medicalsupplier = "medical-supplier";
    public static final String laboratory = "laboratory";
    public static final String optrician = "optrician";
    public static final String beauty = "beauty";
    public static final String id = "id";
    public static final String comment = "comment";
    public static final String categories = "categories";
    public static final String profile_id = "profile_id";
    public static final String type = "type";
    public static final String region_id = "region_id";
    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String user_id = "user_id";
    public static final String category_id = "category_id";
    public static final String title = "title";
    public static final String category = "category";
    public static final String fax = "fax";
    public static final String phone = "phone";
    public static final String address = "address";
    public static final String stars = "stars";
    public static final String id_types = "id_types";
    public static final String regions = "regions";
    public static final String id_type_id = "id_type_id";
    public static final String id_number = "id_number";
    public static final String opening_days = "opening_days";
    public static final String closing_time = "closing_time";
    public static final String opening_time = "opening_time";
    public static final String user = "user";
    public static final String next_page_url = "next_page_url";
    public static final String receiver_id = "receiver_id";
    public static final String subject = "subject";
    public static final String body = "body";
    public static final String read_count = "read_count";
    public static final String allow_comments = "allow_comments";
    public static final String created_at = "created_at";
    public static final String requested_date = "requested_date";
    public static final String reservation_id = "reservation_id";
    public static final String requested_time = "requested_time";
    public static final String notes = "notes";
    public static final String resource_id = "resource_id";
    public static final String uuid = "uuid";
    public static final String gender = "gender";
    public static final String consultation_fee = "consultation_fee";
    public static final String viewable = "viewable";
    public static final String is_featured = "is_featured";
    public static final String anonymous = "anonymous";
    public static final String author = "author";
    public static final String article_id = "article_id";
    public static final String comments = "comments";
    public static final String updated_at = "updated_at";
    public static final String comment_id = "comment_id";
    public static final String end_date = "end_date";
    public static final String start_date = "start_date";
    public static final String publish = "publish";
    public static final String pic = "pic";
    public static final String confirmed = "confirmed";
    public static final String rejected = "rejected";
    public static final String pharmacies = "pharmacies";
    public static final String sender_id = "sender_id";
    public static final String read = "read";
    public static final String sender_name = "sender_name";
    public static final String recipient_name = "recipient_name";
    public static final String sender_avatar = "sender_avatar";
    public static final String recipient_avatar = "recipient_avatar";
    public static final String notifications = "notifications";

    public static final String notificationType = "notificationType";
    public static final String appointmentDate = "appointmentDate";
    public static final String appointmentTime = "appointmentTime";
    public static final String appointmentDoctor = "appointmentDoctor";

    public static final String IMAGE_PATH  = "https://www.sahtek.com";

}
