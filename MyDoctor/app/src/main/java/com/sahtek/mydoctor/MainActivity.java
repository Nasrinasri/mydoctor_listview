package com.sahtek.mydoctor;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.fragments.AdviceFragment;
import com.sahtek.mydoctor.fragments.ClinickFragment;
import com.sahtek.mydoctor.fragments.ForumFragment;
import com.sahtek.mydoctor.fragments.FreeFragment;
import com.sahtek.mydoctor.fragments.HomeFragment;
import com.sahtek.mydoctor.models.Category;
import com.sahtek.mydoctor.models.Notif;
import com.sahtek.mydoctor.models.Profil;
import com.sahtek.mydoctor.models.Region;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;
import com.sahtek.mydoctor.utils.Tools;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static ArrayList<Region> ListRegion = new ArrayList<Region>();
    public static ArrayList<Category> ListCategory = new ArrayList<Category>();
    LinearLayout bottomtab_free, bottomtab_clinic, bottomtab_advice, bottomtab_forum, bottomtab_home;
    int view_position = 0;
    boolean top_selected = false;
    Fragment HomeSubFragment = null;
    private InterstitialAd mInterstitialAd;
    //show fullscreen after 60 secondes
    private Handler disconnectHandler = new Handler();
    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {

            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        }
    };
    private TextView notificaionCountMenuText,messageCountMenuText;
    public  static  String rsp = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Tools.systemBarLolipop(this);

        MobileAds.initialize(this, getString(R.string.ad_app_id));
        final AdView mAdView = findViewById(R.id.adView);
        mAdView.setVisibility(View.GONE);
        AdRequest adRequest = new AdRequest
                .Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdLeftApplication() {
            }

            @Override
            public void onAdClosed() {
            }
        });
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id_release));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build());
        disconnectHandler.postDelayed(disconnectCallback, AppConstants.DISCONNECT_TIMEOUT);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_bar);

        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

        IntialiseView();
        AddActionClickLintener();

        GetListRegion();
        GetListCategory();
        getUser();
    }

    private void IntialiseView() {

        bottomtab_home = (LinearLayout) findViewById(R.id.bottomtab_home);
        bottomtab_forum = (LinearLayout) findViewById(R.id.bottomtab_forum);
        bottomtab_advice = (LinearLayout) findViewById(R.id.bottomtab_advice);
        bottomtab_clinic = (LinearLayout) findViewById(R.id.bottomtab_clinic);
        bottomtab_free = (LinearLayout) findViewById(R.id.bottomtab_free);

        HomeFragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, fragment);
        fragmentTransaction.commit();
        UpdateActivedView();


    }

    private void AddActionClickLintener() {

        bottomtab_free.setOnClickListener(this);
        bottomtab_clinic.setOnClickListener(this);
        bottomtab_advice.setOnClickListener(this);
        bottomtab_forum.setOnClickListener(this);
        bottomtab_home.setOnClickListener(this);

    }

    public void UpdateView(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, fragment);
        fragmentTransaction.commit();
        top_selected = true;
        HomeSubFragment = fragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nmain, menu);
        MenuItem item = menu.findItem(R.id.action_notif);
        MenuItem item_message = menu.findItem(R.id.action_message);
        item.setActionView(R.layout.notification_item_with_item_count);
        item_message.setActionView(R.layout.messahe_with_count);

        item_message.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(MainActivity.this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        item.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent notif = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(notif);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        RelativeLayout bookingCount = (RelativeLayout) item.getActionView();
        RelativeLayout messageCount = (RelativeLayout) item_message.getActionView();

        notificaionCountMenuText = bookingCount.findViewById(R.id.notification_count);
        messageCountMenuText = messageCount.findViewById(R.id.message_count);
        notificaionCountMenuText.setVisibility(View.GONE);
        messageCountMenuText.setVisibility(View.GONE);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                AppUtility.showFilterPopup(findViewById(R.id.action_settings), this);
                return true;

            case R.id.action_message:
                Intent ii = new Intent(this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;

            case R.id.action_notif:
                Intent notif = new Intent(this, NotificationActivity.class);
                startActivity(notif);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        Fragment fragment = null;
        if (view.getId() == R.id.bottomtab_home) {
            fragment = new HomeFragment();
            view_position = 0;
        }
        if (view.getId() == R.id.bottomtab_forum) {
            fragment = new ForumFragment();
            view_position = 1;
        }
        if (view.getId() == R.id.bottomtab_advice) {
            fragment = new AdviceFragment();
            view_position = 2;
        }
        if (view.getId() == R.id.bottomtab_clinic) {
            fragment = new ClinickFragment();
            view_position = 3;
        }
        if (view.getId() == R.id.bottomtab_free) {
            fragment = new FreeFragment();
            view_position = 4;
        }

        if (fragment != null) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.contentContainer, fragment);
            fragmentTransaction.commit();
            UpdateActivedView();

        }


    }

    private void UpdateActivedView() {
        bottomtab_free.setActivated(false);
        bottomtab_clinic.setActivated(false);
        bottomtab_advice.setActivated(false);
        bottomtab_forum.setActivated(false);
        bottomtab_home.setActivated(false);
        top_selected = false;

        switch (view_position) {
            case 0:
                bottomtab_home.setActivated(true);
                break;
            case 1:
                bottomtab_forum.setActivated(true);
                break;

            case 2:
                bottomtab_advice.setActivated(true);
                break;
            case 3:
                bottomtab_clinic.setActivated(true);
                break;
            case 4:
                bottomtab_free.setActivated(true);
                break;
        }

    }

    @Override
    public void onBackPressed() {

        if ((view_position == 0 && top_selected) || view_position != 0) {
            bottomtab_home.performClick();
            top_selected = false;
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 700: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (top_selected) {
                        if (HomeSubFragment != null) {
                            HomeSubFragment.onResume();
                        }
                    }
                } else {
                    Log.i("Permission", "Permission denied by user.");
                }
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void GetListRegion() {

        String Connect_URL = AppConstants.Region_URL;


        AppRestClient.get(Connect_URL, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {
                    ListRegion.clear();
                    ListRegion.addAll(ResponseParser.parseRegionResponse(response));

                } else {

                }
            }

        });

    }

    private void GetListCategory() {
        String Connect_URL = AppConstants.Category_URL;

        AppRestClient.get(Connect_URL, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {
                    ListCategory.clear();
                    ListCategory.addAll(ResponseParser.parseCategoryResponse(response));

                } else {

                }
            }
        });
    }

    private void getUser() {

        String Connect_URL = AppConstants.GET_PROFIL_URL;

        AppRestClient.get(Connect_URL, new SessionManager(this).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("responseuser", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {
                    Profil profil = ResponseParser.parseLoggingProfileResponse(response);
                    new SessionManager(MainActivity.this).setUserId(profil.getUserId());
                } else {
                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void GetListNotifs() {
        String Connect_URL = "";
        String prefix = "notifications";

        Connect_URL = AppRestClient.BASE_URL + prefix;

        AppRestClient.getFirst(Connect_URL, new SessionManager(this).getAccessToken(), new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(MainActivity.this, errorResponse.toString(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(MainActivity.this, getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                //nextPage = ResponseParser.parseMedNextPage(response);
                if (rsp.equals("+OK")) {
                    ArrayList<Notif> notifs = ResponseParser.parseListNotifsResponse(response);
                    if (notificaionCountMenuText != null) {
                        if (notifs.size() > 0) {
                            notificaionCountMenuText.setText(String.valueOf(notifs.size()));
//                            notificaionCountMenuText.setVisibility(View.VISIBLE);
                            notificaionCountMenuText.setVisibility(View.GONE);
                        } else {
                            notificaionCountMenuText.setVisibility(View.GONE);
                        }

                    }

                } else {
                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disconnectHandler.removeCallbacks(disconnectCallback);
    }


    private void GetMessageNumber() {

        String profile_id = new SessionManager(MainActivity.this).getUserId();
        String Connect_URL = "https://www.sahtek.com/web/messages/api/new/" + profile_id;

        AppRestClient.externGet(Connect_URL, new SessionManager(MainActivity.this).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(MainActivity.this,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(MainActivity.this,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(MainActivity.this,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Log.e("response", "response " + response.toString());
                 rsp = ResponseParser.parseNbMessageResponse(response);
                messageCountMenuText.setText(rsp);
                if(!rsp.equals("0")) {
                    messageCountMenuText.setVisibility(View.VISIBLE);
                } else {
                    messageCountMenuText.setVisibility(View.GONE);
                }
            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        GetMessageNumber();
    }
}
