package com.sahtek.mydoctor.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.DoctorDetailsActivity;
import com.sahtek.mydoctor.PharmayDetailsActivity;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.ToolsLabDetailActivity;
import com.sahtek.mydoctor.adapters.DoctorAdapter;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class MyCliniqueFragment extends Fragment {
    GridView gridview;
    public DoctorAdapter FavorisAdapter;
    TextView search_ed;
    public ArrayList<BusinessProfil> profils = new ArrayList<BusinessProfil>();
    Context _context;
    String category = "";
    ProgressBar pbr;
    private static ProgressDialog _progress;


    public MyCliniqueFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MyCliniqueFragment newInstance() {
        MyCliniqueFragment fragment = new MyCliniqueFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        _context = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_clinique, container, false);
        InitialiseView(rootView);
        AddClickListenner();
        getFavoris();
        return rootView;
    }

    private void InitialiseView(View rootView) {

        gridview = (GridView) rootView.findViewById(R.id.gridview);
        pbr = (ProgressBar) rootView.findViewById(R.id.pbr);
        search_ed = (TextView) rootView.findViewById(R.id.search_ed);

    }

    private void getFavoris() {

        profils = new ArrayList<BusinessProfil>();
        FavorisAdapter = new DoctorAdapter(_context, profils);
        FavorisAdapter.favoris = true;
        FavorisAdapter.fragment = this;
        gridview.setAdapter(FavorisAdapter);
        if (AppUtility.isInternetConnected(_context)) {
            GetListFavoris();

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void AddClickListenner() {

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BusinessProfil prof = (BusinessProfil) FavorisAdapter.getItem(i);
                //GetDoctorDetail(prof.getId());
                Intent ii = null;
                if (prof.getType().equals("pharmacy")) {
                    ii = new Intent(_context, PharmayDetailsActivity.class);
                } else if (prof.getType().equals("doctor") || prof.getType().equals("beauty")) {
                    ii = new Intent(getActivity(), DoctorDetailsActivity.class);
                } else {
                    ii = new Intent(getActivity(), ToolsLabDetailActivity.class);
                }

                ii.putExtra("profile_id", prof.getId());
                ii.putExtra("profile", prof);
                getActivity().startActivity(ii);
                getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        search_ed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CategoriesDialog();

            }
        });

    }

    private void GetListFavoris() {
        String Connect_URL = AppConstants.GET_FAVORIS_URL + category;

        AppRestClient.get(Connect_URL, new SessionManager(getActivity()).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onStart() {

                pbr.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                Log.e("response myclinic", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                // nextPage = ResponseParser.parseNextPage(response);
                if (rsp.equals("+OK")) {
                    try {
                        JSONObject data = response.getJSONObject(AppConstants.data);
                        if (data.has(AppConstants.doctor)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.doctor);
                            profils.addAll(ResponseParser.parseFavorisProfilesResponse(profiles));
                        }
                        if (data.has(AppConstants.pharmacy)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.pharmacy);
                            profils.addAll(ResponseParser.parseFavorisProfilesResponse(profiles));
                        }

                        if (data.has(AppConstants.laboratory)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.laboratory);
                            profils.addAll(ResponseParser.parseFavorisProfilesResponse(profiles));
                        }

                        if (data.has(AppConstants.medicalsupplier)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.medicalsupplier);
                            profils.addAll(ResponseParser.parseFavorisProfilesResponse(profiles));
                        }

                        if (data.has(AppConstants.optrician)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.optrician);
                            profils.addAll(ResponseParser.parseFavorisProfilesResponse(profiles));
                        }
                        if (data.has(AppConstants.beauty)) {
                            JSONArray profiles = data.getJSONArray(AppConstants.beauty);
                            profils.addAll(ResponseParser.parseFavorisProfilesResponse(profiles));
                        }

                        FavorisAdapter.notifyDataSetChanged();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }

    public void CategoriesDialog() {

        final String[] items = new String[7];
        items[0] = _context.getString(R.string.all);
        items[1] = _context.getString(R.string.doctors);
        items[2] = _context.getString(R.string.pharmacy);
        items[3] = _context.getString(R.string.labos);
        items[4] = _context.getString(R.string.tools);
        items[5] = _context.getString(R.string.eyewear);
        items[6] = _context.getString(R.string.beauty);

        final ArrayList<String> categorys = new ArrayList<String>();
        categorys.add("");
        categorys.add("?category=doctor");
        categorys.add("?category=pharmacy");
        categorys.add("?category=laboratory");
        categorys.add("?category=medical-supplier");
        categorys.add("?category=optrician");
        categorys.add("?category=beauty");

        View titleView = LayoutInflater.from(_context).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.action_category));

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (!category.equals(categorys.get(item))) {
                    category = categorys.get(item);
                    search_ed.setText(items[item]);
                    getFavoris();
                }

            }
        }).show();

    }


    private void GetDoctorDetail(String profile_id) {

        String Connect_URL = AppConstants.DETAIL_FEATURED_URL + profile_id;


        AppRestClient.get(Connect_URL, new SessionManager(_context).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                hideDialog();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                hideDialog();
            }

            @Override
            public void onStart() {

                displayDialog();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                hideDialog();
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {

                    BusinessProfil prof = ResponseParser.parseSingleBusinessProfilesResponse(response);
                    if(prof != null) {
                        Intent ii = null;
                        if (prof.getType().equals("pharmacy")) {
                            ii = new Intent(_context, PharmayDetailsActivity.class);
                        } else if (prof.getType().equals("doctor") || prof.getType().equals("beauty")) {
                            ii = new Intent(getActivity(), DoctorDetailsActivity.class);
                        } else {
                            ii = new Intent(getActivity(), ToolsLabDetailActivity.class);
                        }

                        ii.putExtra("profile_id", prof.getId());
                        ii.putExtra("profile", prof);
                        getActivity().startActivity(ii);
                        getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    }

                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }

    private void displayDialog() {
        _progress = new ProgressDialog(_context);
        _progress.setMessage(_context.getString(R.string.waitmessage));
        _progress.show();
        _progress.setCanceledOnTouchOutside(false);
        _progress.setCancelable(false);
    }

    private void hideDialog() {

        _progress.dismiss();
    }



}
