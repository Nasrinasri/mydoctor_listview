package com.sahtek.mydoctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.adapters.ListPharmacyAdapter;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class FindPaharmacyActivity extends AppCompatActivity {

    ListView med_list;
    ListPharmacyAdapter adapter;

    ArrayList<BusinessProfil> _listMed = new ArrayList<BusinessProfil>();
    ProgressBar pbr;
    String nextPage = "null";
    private boolean loadingMore = false;
    RelativeLayout list_footer;
    boolean first = true;
    EditText search_ed;
    String med_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_paharmacy);
        setupActionBar();
        IntialiseView();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            med_id = extras.getString("med_id");
        }

        if (AppUtility.isInternetConnected(FindPaharmacyActivity.this)) {
            GetListMed();

        } else {
            Toast.makeText(FindPaharmacyActivity.this,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }

    }

    private void IntialiseView() {

        med_list = (ListView) findViewById(R.id.med_list);

        pbr = (ProgressBar) findViewById(R.id.pbr);
        list_footer = (RelativeLayout) findViewById(R.id.list_footer);
        search_ed = (EditText) findViewById(R.id.search_ed);

        adapter = new ListPharmacyAdapter(FindPaharmacyActivity.this, _listMed);
        med_list.setAdapter(adapter);

        search_ed.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String name = search_ed.getText().toString().trim();
                FiltreMed(name);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1,
                                      int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
        });

        med_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent ii = new Intent(FindPaharmacyActivity.this, PharmayDetailsActivity.class);
                BusinessProfil prof = (BusinessProfil) adapter.getItem(i);
                ii.putExtra("profile_id", prof.getId());
                ii.putExtra("profile", prof);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

    }

    private void FiltreMed(String name) {
        first = true;
        _listMed.clear();
        adapter.notifyDataSetChanged();
        if (AppUtility.isInternetConnected(FindPaharmacyActivity.this)) {
            GetListMed();

        } else {
            Toast.makeText(FindPaharmacyActivity.this,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }


    private void GetListMed() {
        String Connect_URL = "";
        String prefix = "medicine/pharmacy?medicineId=" + med_id;
        if (first) {
            Connect_URL = AppRestClient.BASE_URL + prefix;
        } else {
            Connect_URL = nextPage;
        }


        AppRestClient.getFirst(Connect_URL, new SessionManager(this).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(FindPaharmacyActivity.this,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(FindPaharmacyActivity.this,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(FindPaharmacyActivity.this,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onStart() {

                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                } else {
                    list_footer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                //nextPage = ResponseParser.parseMedNextPage(response);
                if (rsp.equals("+OK")) {
                    _listMed.addAll(ResponseParser.parsePharmacyResponse(response));
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(FindPaharmacyActivity.this,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            // actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setLogo(R.mipmap.ic_bar);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nmain, menu);
        MenuItem item_message = menu.findItem(R.id.action_message);
        item_message.setActionView(R.layout.messahe_with_count);
        RelativeLayout messageCount = (RelativeLayout) item_message.getActionView();
        TextView messageCountMenuText = messageCount.findViewById(R.id.message_count);
        messageCountMenuText.setText(MainActivity.rsp);
        if(MainActivity.rsp.equals("0")) {
            messageCountMenuText.setVisibility(View.GONE);
        } else {
            messageCountMenuText.setVisibility(View.VISIBLE);
        }


        item_message.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(FindPaharmacyActivity.this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                AppUtility.showFilterPopup(findViewById(R.id.action_settings), this);
                return true;

            case R.id.action_message:
                Intent ii = new Intent(this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;

            case R.id.action_notif:
                Intent notif = new Intent(this, NotificationActivity.class);
                startActivity(notif);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
