package com.sahtek.mydoctor;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ForgotPasswordActivity extends AppCompatActivity {

    TextView cancel_bt, send_bt;
    EditText email_ed, validation_code_ed, password_ed, repassword_ed;
    TextView message;
    int step = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setupActionBar();
        InitialiseView();
        AddClickListenner();

    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void InitialiseView() {

        cancel_bt = (TextView) findViewById(R.id.cancel_bt);
        send_bt = (TextView) findViewById(R.id.send_bt);

        email_ed = (EditText) findViewById(R.id.email_ed);
        validation_code_ed = (EditText) findViewById(R.id.validation_code_ed);
        password_ed = (EditText) findViewById(R.id.password_ed);
        repassword_ed = (EditText) findViewById(R.id.repassword_ed);
        message = (TextView) findViewById(R.id.message);
    }

    private void AddClickListenner() {

        cancel_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        send_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = email_ed.getText().toString().trim();
                String validation_code = validation_code_ed.getText().toString().trim();
                String password = password_ed.getText().toString().trim();
                String repassword = repassword_ed.getText().toString().trim();

                if (step == 1) {

                    if (AppUtility.isInternetConnected(ForgotPasswordActivity.this)) {
                        if (TextUtils.isEmpty(email)) {
                            email_ed.setError(getString(R.string.empty_email_error));
                        } else if (!(AppUtility.isValidEmail(email))) {
                            email_ed.setError(getString(R.string.ivalid_email_format));
                        } else {
                            Step1(email);

                        }

                    } else {
                        Toast.makeText(ForgotPasswordActivity.this,
                                R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                                .show();
                    }

                } else {

                    if (TextUtils.isEmpty(validation_code)) {
                        validation_code_ed.setError(getString(R.string.validation_code_err));
                    } else if (TextUtils.isEmpty(password)) {
                        validation_code_ed.setError(null);
                        password_ed.setError(getString(R.string.empty_password_error));
                    } else if (TextUtils.isEmpty(repassword)) {
                        validation_code_ed.setError(null);
                        password_ed.setError(null);
                        repassword_ed.setError(getString(R.string.empty_password_error));
                    } else if (!(password.equals(repassword))) {
                        validation_code_ed.setError(null);
                        password_ed.setText("");
                        repassword_ed.setText("");
                        password_ed.setError(getString(R.string.password_not_match_error));
                        repassword_ed.setError(getString(R.string.password_not_match_error));
                    } else {

                        Step2(email, password, validation_code);

                    }

                }
            }
        });
    }


    private void Step1(final String email) {

        JSONObject jdata = new JSONObject();
        String Connect_URL = AppConstants.FORGOT_URL;
        try {
            jdata.put(AppConstants.email, email);
        } catch (Exception ex) {
        }
        StringEntity entity;

        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.post(getApplication(), Connect_URL, entity, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    Toast.makeText(getApplicationContext(),
                            getString(R.string.connexion_problem), Toast.LENGTH_SHORT)
                            .show();


                    EnableDisable(true);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {

                    Toast.makeText(getApplicationContext(),
                            getString(R.string.connexion_problem), Toast.LENGTH_SHORT)
                            .show();
                    EnableDisable(true);
                }

                @Override
                public void onStart() {
                    EnableDisable(false);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    Log.e("response", "response " + response.toString());
                    EnableDisable(true);
                    email_ed.setVisibility(View.GONE);
                    validation_code_ed.setVisibility(View.VISIBLE);
                    password_ed.setVisibility(View.VISIBLE);
                    repassword_ed.setVisibility(View.VISIBLE);
                    email_ed.setVisibility(View.GONE);
                    String token = ResponseParser.parseMessageKeyResponse(response, "token");
                    validation_code_ed.setText(token);
                    step = 2;
                    message.setText(getString(R.string.forget_txt2));

                }

            });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }

    private void EnableDisable(Boolean enable) {
        cancel_bt.setEnabled(enable);
        send_bt.setEnabled(enable);
        email_ed.setEnabled(enable);
        validation_code_ed.setEnabled(enable);
        password_ed.setEnabled(enable);
        repassword_ed.setEnabled(enable);

    }


    private void Step2(final String email, String password, String token) {

        JSONObject jdata = new JSONObject();
        String Connect_URL = AppConstants.RESET_URL;
        try {
            jdata.put(AppConstants.email, email);
            jdata.put(AppConstants.password, password);
            jdata.put(AppConstants.password_confirmation, password);
            jdata.put(AppConstants.token, token);
        } catch (Exception ex) {
        }
        StringEntity entity;
        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.post(getApplication(), Connect_URL, entity, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    Toast.makeText(getApplicationContext(),
                            getString(R.string.connexion_problem), Toast.LENGTH_SHORT)
                            .show();


                    EnableDisable(true);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {

                    Toast.makeText(getApplicationContext(),
                            getString(R.string.connexion_problem), Toast.LENGTH_SHORT)
                            .show();
                    EnableDisable(true);
                }

                @Override
                public void onStart() {
                    EnableDisable(false);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    Log.e("response", "response " + response.toString());
                    EnableDisable(true);
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.succ_up_pass), Toast.LENGTH_SHORT)
                            .show();

                    finish();
                }

            });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }
}
