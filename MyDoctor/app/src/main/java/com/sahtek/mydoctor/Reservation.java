package com.sahtek.mydoctor;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.fragments.ReservationFragment;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.nio.charset.UnsupportedCharsetException;
import java.util.Calendar;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

public class Reservation extends Dialog implements
        View.OnClickListener {

    public Button cancel_bt, send_bt;
    public Boolean Update = false;
    public com.sahtek.mydoctor.models.Reservation reservation = null;
    public ReservationFragment fragment = null;
    TextView date_value, hour_value;
    String receiver_id = "";
    ProgressBar progressBar1;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    private Context _context;
    private EditText notes;
    private int years;
    private int month;
    private int day;
    private int hoursDebut;
    private int minutesDebut;
    private String date_to_send = "";

    public Reservation(Context context, String receiver_id) {
        super(context);
        _context = context;
        this.receiver_id = receiver_id;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.reservation);
        setCanceledOnTouchOutside(false);
        initilize();

        initilizeEvent();

    }


    private void initilize() {
        notes = (EditText) findViewById(R.id.notes);
        hour_value = (TextView) findViewById(R.id.hour_value);
        date_value = (TextView) findViewById(R.id.date_value);

        cancel_bt = (Button) findViewById(R.id.cancel_bt);
        send_bt = (Button) findViewById(R.id.send_bt);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

        Calendar calendar = Calendar.getInstance(Locale.getDefault());

        if (Update && reservation != null) {
            if (!reservation.getNotes().equals("null")) {
                notes.setText(reservation.getNotes());
            }
            String OldHeure = reservation.getRequested_time();
            if (OldHeure.length() > 4) {
                OldHeure = OldHeure.substring(0, 5);
            }
            hour_value.setText(OldHeure);
            date_value.setText(reservation.getRequested_date());
            date_to_send = (reservation.getRequested_date());
        }


        years = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        String days = day + "";
        String months = (month + 1) + "";
        if (months.length() == 1)
            months = "0" + months;
        if (days.length() == 1)
            days = "0" + days;

        if (!(Update && reservation != null)) {
            date_value.setText(years + "-" + months + "-" + days);
            date_to_send = years + "-" + months + "-" + days;
        }

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);

        hoursDebut = hour;
        minutesDebut = minutes;
        String hours = hour + "";
        String minute = minutes + "";
        if (hours.length() == 1)
            hours = "0" + hours;
        if (minute.length() == 1)
            minute = "0" + minute;

        if (!(Update && reservation != null)) {
            hour_value.setText(hours + ":" + minute);
        }

    }


    private void initilizeEvent() {
        cancel_bt.setOnClickListener(this);
        send_bt.setOnClickListener(this);
        date_value.setOnClickListener(this);
        hour_value.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_bt:
                dismiss();

                break;
            case R.id.send_bt:

                if (AppUtility.isInternetConnected(_context)) {

                    String note = notes.getText().toString().trim();

                    if (Update) {
                        Update(note);

                    } else {
                        Send(note);
                    }


                } else {
                    Toast.makeText(_context,
                            R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                            .show();

                }

                break;
            case R.id.date_value:
                showDatePicker();
                break;
            case R.id.hour_value:
                showTimePicker();
                break;

            default:
                break;
        }

    }

    private void Send(final String note) {
        JSONObject jdata = new JSONObject();
        try {
            jdata.put(AppConstants.profile_id, receiver_id);
            jdata.put(AppConstants.requested_date, date_to_send);
            jdata.put(AppConstants.requested_time, hour_value.getText());
            jdata.put(AppConstants.notes, note);

        } catch (Exception ex) {
        }
        StringEntity entity;
        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.postAutorized(_context.getApplicationContext(),
                    AppConstants.RESERVATION_URL, new SessionManager(_context).getAccessToken(), entity,
                    "application/json", new JsonHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            if (errorResponse != null) {
                                Toast.makeText(_context,
                                        errorResponse.toString(), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(_context,
                                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                        .show();

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            Toast.makeText(_context,
                                    _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onStart() {

                            progressBar1.setVisibility(View.VISIBLE);
                            EnableDisable(false);

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.e("response", "response " + response.toString());
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            String rsp = ResponseParser.parseResultResponse(response);
                            String msg = ResponseParser.parseMessageResponse(response);
                            if (rsp.equals("+OK")) {
                                Toast.makeText(_context,
                                        _context.getString(R.string.successresrv), Toast.LENGTH_SHORT)
                                        .show();
                                notes.setText("");
                                dismiss();

                            } else {
                                Toast.makeText(_context,
                                        msg, Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }

                    });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }


    private void Update(final String note) {
        JSONObject jdata = new JSONObject();
        try {
            jdata.put(AppConstants.reservation_id, reservation.getId());
            jdata.put(AppConstants.requested_date, date_to_send);
            jdata.put(AppConstants.requested_time, hour_value.getText());
            jdata.put(AppConstants.notes, note);

        } catch (Exception ex) {
        }
        StringEntity entity;
        try {
            entity = new StringEntity(jdata.toString(), ContentType.APPLICATION_JSON);
            AppRestClient.postAutorized(_context.getApplicationContext(),
                    AppConstants.EDIT_RESERVATION_URL, new SessionManager(_context).getAccessToken(), entity,
                    "application/json", new JsonHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            if (errorResponse != null) {
                                Toast.makeText(_context,
                                        errorResponse.toString(), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(_context,
                                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                        .show();

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            Toast.makeText(_context,
                                    _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onStart() {

                            progressBar1.setVisibility(View.VISIBLE);
                            EnableDisable(false);

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.e("response", "response " + response.toString());
                            progressBar1.setVisibility(View.INVISIBLE);
                            EnableDisable(true);
                            String rsp = ResponseParser.parseResultResponse(response);
                            String msg = ResponseParser.parseMessageResponse(response);
                            if (rsp.equals("+OK")) {
                                Toast.makeText(_context,
                                        _context.getString(R.string.successresrv), Toast.LENGTH_SHORT)
                                        .show();
                                if (fragment != null) {
                                    fragment.GetListReservations();
                                }

                                dismiss();

                            } else {
                                Toast.makeText(_context,
                                        msg, Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }

                    });
        } catch (UnsupportedCharsetException e) {
            e.printStackTrace();
        }
    }

    private void EnableDisable(boolean enable) {
        cancel_bt.setEnabled(enable);
        send_bt.setEnabled(enable);
        notes.setEnabled(enable);
        date_value.setEnabled(enable);
        hour_value.setEnabled(enable);
    }


    private void showDatePicker() {

        if (datePickerDialog != null) {
            datePickerDialog.dismiss();
            datePickerDialog = null;
        }

        datePickerDialog = new DatePickerDialog(_context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.set(year, monthOfYear, dayOfMonth);
                years = year;
                month = monthOfYear;
                day = dayOfMonth;
                String days = dayOfMonth + "";
                String months = (monthOfYear + 1) + "";
                if (months.length() == 1)
                    months = "0" + months;
                if (days.length() == 1)
                    days = "0" + days;
                date_value.setText(years + "-" + months + "-" + days);
                date_to_send = years + "-" + months + "-" + days;

            }
        }, years, month, day);


        datePickerDialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                datePickerDialog = null;

            }
        });


        datePickerDialog.show();
    }


    private void showTimePicker() {

        if (timePickerDialog != null) {
            timePickerDialog.dismiss();
            timePickerDialog = null;
        }

        int housrs = 0;
        int minutes = 0;

        housrs = hoursDebut;
        minutes = minutesDebut;

        timePickerDialog = new TimePickerDialog(_context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                hoursDebut = selectedHour;
                minutesDebut = selectedMinute;
                String hours = hoursDebut + "";
                String minute = minutesDebut + "";
                if (hours.length() == 1)
                    hours = "0" + hours;
                if (minute.length() == 1)
                    minute = "0" + minute;
                hour_value.setText(hours + ":" + minute);


            }
        }, housrs,
                minutes, DateFormat.is24HourFormat(_context));


        timePickerDialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                timePickerDialog = null;

            }
        });

        timePickerDialog.show();
    }

}
