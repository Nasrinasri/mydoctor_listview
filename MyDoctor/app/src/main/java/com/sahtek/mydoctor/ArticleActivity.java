package com.sahtek.mydoctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.adapters.CommentAdapter;
import com.sahtek.mydoctor.models.Article;
import com.sahtek.mydoctor.models.Comment;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.sahtek.mydoctor.utils.AppUtility.loadPhoto;


public class ArticleActivity extends AppCompatActivity {

    public CommentAdapter adapter;
    public ArrayList<Comment> ListComment = new ArrayList<Comment>();
    Article article = null;
    TextView name, sepliciality, description, title, nb_view;
    LinearLayout topaction_rate, topaction_comment;
    TextView comment_empty;
    ProgressBar pbr;
    ListView list;
    boolean first = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        setupActionBar();
        name = (TextView) findViewById(R.id.name);
        sepliciality = (TextView) findViewById(R.id.sepliciality);
        description = (TextView) findViewById(R.id.description);
        title = (TextView) findViewById(R.id.title);
        nb_view = (TextView) findViewById(R.id.nb_view);
        comment_empty = (TextView) findViewById(R.id.comment_empty);
        list = (ListView) findViewById(R.id.list);
        pbr = (ProgressBar) findViewById(R.id.pbr);

        topaction_comment = (LinearLayout) findViewById(R.id.topaction_comment);
        topaction_rate = (LinearLayout) findViewById(R.id.topaction_rate);


        AddClickListenner();

        Bundle extras = getIntent().getExtras();

        Intent mIntent = getIntent();
        article = (Article) mIntent.getParcelableExtra("article");
        String articleId = "";
        if (article != null) {
            articleId = article.getId();
        }
        adapter = new CommentAdapter(ArticleActivity.this, ListComment, articleId);
        list.setAdapter(adapter);
        if (article != null) {
            ImageView img = findViewById(R.id.img);
            loadPhoto(this, article.getAuthor().getAvatar(), img);

            name.setText(article.getAuthor().getName());
            sepliciality.setText(article.getCreated_at());
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                description.setText(Html.fromHtml(article.getBody(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                description.setText(Html.fromHtml(article.getBody()));
            }
            title.setText(article.getTitle());
            nb_view.setText(article.getRead_count() + "");
            if (article.getAllow_comments().equals("0")) {
                comment_empty.setText(getString(R.string.add_comment_not));
                list.setEmptyView(comment_empty);
            } else {
                if (AppUtility.isInternetConnected(ArticleActivity.this)) {
                    GetComment(article.getId());

                } else {
                    Toast.makeText(ArticleActivity.this,
                            R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                            .show();

                }

            }


            if (AppUtility.isInternetConnected(ArticleActivity.this)) {
                ReadArticle(article.getId());

            } else {
                Toast.makeText(ArticleActivity.this,
                        R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                        .show();

            }
        }

    }


    private void AddClickListenner() {


        topaction_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (article.getAllow_comments().equals("0")) {
                    Toast.makeText(ArticleActivity.this,
                            getString(R.string.add_comment_not), Toast.LENGTH_SHORT)
                            .show();
                } else {
                    ArticleComment dialog = new ArticleComment(ArticleActivity.this, article.getId());
                    dialog.show();

                }
            }
        });


        topaction_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateArticle dialog = new RateArticle(ArticleActivity.this, article.getId());
                dialog.show();
            }
        });


    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            // actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setLogo(R.mipmap.ic_bar);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nmain, menu);
        MenuItem item_message = menu.findItem(R.id.action_message);
        item_message.setActionView(R.layout.messahe_with_count);
        RelativeLayout messageCount = (RelativeLayout) item_message.getActionView();
        TextView messageCountMenuText = messageCount.findViewById(R.id.message_count);
        messageCountMenuText.setText(MainActivity.rsp);
        if(MainActivity.rsp.equals("0")) {
            messageCountMenuText.setVisibility(View.GONE);
        } else {
            messageCountMenuText.setVisibility(View.VISIBLE);
        }


        item_message.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(ArticleActivity.this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                AppUtility.showFilterPopup(findViewById(R.id.action_settings), this);
                return true;

            case R.id.action_message:
                Intent ii = new Intent(this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;

            case R.id.action_notif:
                Intent notif = new Intent(this, NotificationActivity.class);
                startActivity(notif);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void ReadArticle(String article_id) {

        String Connect_URL = AppConstants.READ_ARTICLE_URL + "?article_id=" + article_id;

        AppRestClient.get(Connect_URL, new SessionManager(this).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(ArticleActivity.this,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(ArticleActivity.this,
                            getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(ArticleActivity.this,
                        getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("article_id", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {

                    String nb = ResponseParser.parseArticleNbView(response);
                    if (!nb.isEmpty()) {
                        nb_view.setText(nb + "");
                    }

                } else {
                    Toast.makeText(ArticleActivity.this,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }


    public void GetComment(String article_id) {

        String Connect_URL = AppConstants.GET_COMMENT_ARTICLE_URL + "?article_id=" + article_id;


        AppRestClient.get(Connect_URL, new SessionManager(this).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                pbr.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                pbr.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onStart() {
                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                pbr.setVisibility(View.INVISIBLE);
                if (msg.equals("+OK")) {
                    ListComment.clear();
                    ListComment.addAll(ResponseParser.parseCommentResponse(response));
                    adapter.notifyDataSetChanged();
                    setListViewHeightBasedOnChildren();
                    list.setEmptyView(comment_empty);

                }
            }

        });

    }

    private void setListViewHeightBasedOnChildren() {
        ListAdapter listAdapter = list.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, list);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = list.getLayoutParams();
        params.height = totalHeight
                + (list.getDividerHeight() * (listAdapter.getCount()));
        list.setLayoutParams(params);
    }

}
