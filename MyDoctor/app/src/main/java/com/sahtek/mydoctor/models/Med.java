package com.sahtek.mydoctor.models;

/**
 * Created by nasri on 29/09/2017.
 */

public class Med {

    private String id;
    private String name;
    private String price;
    private String description;
    private String pic;

    public Med(String id, String name, String description, String pic) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.pic = pic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
