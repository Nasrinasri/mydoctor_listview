package com.sahtek.mydoctor.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.ArticleActivity;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.adapters.AdviceAdapter;
import com.sahtek.mydoctor.models.Article;
import com.sahtek.mydoctor.models.Category;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class AdviceFragment extends Fragment {

    TextView search_ed;
    ProgressBar pbr;
    RelativeLayout list_footer;
    ListView list;
    Context _context;
    String category = "";
    ArrayList<Category> ListCategory = new ArrayList<Category>();
    ArrayList<Article> ListArticle = new ArrayList<Article>();
    boolean first = true;
    String nextPage = "null";
    private boolean loadingMore = false;
    AdviceAdapter adapter;
    private SessionManager sessionManager;

    public AdviceFragment() {
        // Required empty public constructor
    }

    public static AdviceFragment newInstance() {
        AdviceFragment fragment = new AdviceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        _context = getActivity();
        sessionManager = new SessionManager(_context);
        if (AppUtility.isInternetConnected(_context)) {
            GetListCategory();

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_advanced, container, false);
        InitialiseView(rootView);
        AddClickListenner();
        return rootView;
    }

    private void InitialiseView(View rootView) {

        list = (ListView) rootView.findViewById(R.id.list);
        pbr = (ProgressBar) rootView.findViewById(R.id.pbr);
        search_ed = (TextView) rootView.findViewById(R.id.search_ed);
        list_footer = (RelativeLayout) rootView.findViewById(R.id.list_footer);

        adapter = new AdviceAdapter(_context, ListArticle);
        list.setAdapter(adapter);

    }

    private void AddClickListenner() {
        search_ed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CategoriesDialog();
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent ii = new Intent(getActivity(), ArticleActivity.class);
                Article article = (Article) adapter.getItem(i);
                ii.putExtra("article", article);
                getActivity().startActivity(ii);
                getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int currentFirstVisPos = view.getFirstVisiblePosition();
                if (!nextPage.equals("null") && !nextPage.equals("")) {

                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    if ((lastInScreen == totalItemCount) && !(loadingMore)) {

                        if (AppUtility.isInternetConnected(getActivity())) {
                            loadingMore = true;
                            GetListListArticle();

                        } else {

                            Toast.makeText(getActivity(),
                                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                                    .show();
                        }
                    }


                }

            }
        });

    }

    private void FiltreArticle() {
        first = true;
        ListArticle.clear();
        adapter.notifyDataSetChanged();
        if (AppUtility.isInternetConnected(_context)) {
            GetListListArticle();

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }

    private void GetListListArticle() {
        String Connect_URL = "";

        if (first) {
            Connect_URL = AppRestClient.BASE_URL + AppConstants.ARTICLE_URL;
            Connect_URL = Connect_URL + "" + category;
        } else {
            Connect_URL = nextPage;
        }


        AppRestClient.getFirst(Connect_URL, sessionManager.getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onStart() {

                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                } else {
                    list_footer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                nextPage = ResponseParser.parseArticlesNextPage(response);
                if (rsp.equals("+OK")) {

                    ListArticle.addAll(ResponseParser.parseArticleResponse(response));
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }


    private void GetListCategory() {

        String Connect_URL = AppConstants.ADVICE_Category_URL;


        AppRestClient.get(Connect_URL, new SessionManager(getActivity()).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                pbr.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                pbr.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onStart() {
                pbr.setVisibility(View.VISIBLE);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                if (rsp.equals("+OK")) {
                    ListCategory = new ArrayList<Category>();
                    ListCategory.addAll(ResponseParser.parseAdviceCategoryResponse(response));
                    if (ListCategory.size() > 0) {
                        category = "?category=" + ListCategory.get(0).getId();
                        search_ed.setText(ListCategory.get(0).getName());
                        GetListListArticle();

                    } else {
                        pbr.setVisibility(View.INVISIBLE);
                    }

                } else {
                    pbr.setVisibility(View.INVISIBLE);

                }
            }

        });

    }

    public void CategoriesDialog() {

        final String[] items = new String[ListCategory.size()];
        for (int i = 0; i < ListCategory.size(); i++) {
            items[i] = ListCategory.get(i).getName();

        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View titleView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.action_category));

        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (!category.equals("?category=" + ListCategory.get(item).getId())) {
                    category = "?category=" + ListCategory.get(item).getId();
                    search_ed.setText(items[item]);
                    FiltreArticle();
                }

            }
        }).show();

    }


}
