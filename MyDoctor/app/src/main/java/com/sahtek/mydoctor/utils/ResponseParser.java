package com.sahtek.mydoctor.utils;


import com.sahtek.mydoctor.models.Article;
import com.sahtek.mydoctor.models.Author;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.models.Category;
import com.sahtek.mydoctor.models.Comment;
import com.sahtek.mydoctor.models.Med;
import com.sahtek.mydoctor.models.MessageItem;
import com.sahtek.mydoctor.models.Notif;
import com.sahtek.mydoctor.models.Profil;
import com.sahtek.mydoctor.models.Promotions;
import com.sahtek.mydoctor.models.Region;
import com.sahtek.mydoctor.models.Reservation;
import com.sahtek.mydoctor.models.Type;
import com.sahtek.mydoctor.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ResponseParser {


    public static String parseResultResponse(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            status_logging = jObjectComment.getString(AppConstants.rsp);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }

    public static String parseAccessTokenResponse(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            status_logging = jObjectComment.getString(AppConstants.access_token);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }

    public static String parseMessageResponse(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            status_logging = jObjectComment.getString(AppConstants.msg);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }

    public static String parseMessageErrResponse(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            status_logging = jObjectComment.getString(AppConstants.message);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }


    public static String parseMessageKeyResponse(JSONObject jObjectComment, String key) {
        String status_logging = "";
        try {
            status_logging = jObjectComment.getString(key);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }

    public static String parseNextPage(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            JSONObject profiles = jObjectComment.getJSONObject(AppConstants.profiles);
            status_logging = profiles.getString(AppConstants.next_page_url);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }

    public static String parseArticlesNextPage(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            JSONObject data = jObjectComment.getJSONObject(AppConstants.data);
            status_logging = data.getString(AppConstants.next_page_url);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }


   /* public static Profil parseLoggingProfileResponse(JSONObject jObjectComment) {
        JSONObject profileinfos;
        JSONArray libmusicinfos;
        JSONArray scoresinfos;
        Profil profile = null;
        ArrayList<LibMusic> libMusics = new ArrayList<LibMusic>();
        ArrayList<Scores> scores = new ArrayList<Scores>();
        try {
            profileinfos = jObjectComment.getJSONObject(AppConstants.user);
            libmusicinfos = profileinfos.getJSONArray(AppConstants.lib_music);
            scoresinfos = profileinfos.getJSONArray(AppConstants.scores);


            for(int i=0;i<scoresinfos.length();i++)
            {
                if(!scoresinfos.get(i).toString().equals("null"))
                {
                JSONObject item = (JSONObject) scoresinfos.get(i);
                Scores score = new Scores(item.getString(AppConstants._id) , item.getString(AppConstants.place_id) , item.getString(AppConstants.score), item.getString(AppConstants.bestScore) , item.getString(AppConstants.updated_at) );
                scores.add(score);
            }
            }


            for(int i=0;i<libmusicinfos.length();i++)
            {

                if(!libmusicinfos.get(i).toString().equals("null"))
                {  JSONObject item = (JSONObject) libmusicinfos.get(i);
                LibMusic libmusic = new LibMusic(item.getString(AppConstants.lib_name) , item.getString(AppConstants.artiste) , item.getString(AppConstants.title));
                libMusics.add(libmusic);}
            }


            profile = new Profil(profileinfos.getString(AppConstants._id) ,scores ,libMusics);

            if(profileinfos.has(AppConstants.first_name))
            {
                profile.setFirstName(profileinfos.getString(AppConstants.first_name));
            }

            if(profileinfos.has(AppConstants.last_name))
            {
                profile.setLastName(profileinfos.getString(AppConstants.last_name));
            }

            if(profileinfos.has(AppConstants.email))
            {
                profile.setEmail(profileinfos.getString(AppConstants.email));
            }

            if(profileinfos.has(AppConstants.phone))
            {
                profile.setPhone(profileinfos.getString(AppConstants.phone));
            }

            if(profileinfos.has(AppConstants.facebook_id))
            {
                profile.setFacebookID(profileinfos.getString(AppConstants.facebook_id));
            }

            if(profileinfos.has(AppConstants.picture_url))
            {
                profile.setPictureUrl(profileinfos.getString(AppConstants.picture_url));
            }


        } catch (JSONException e) {

            e.printStackTrace();
        }
        return profile;
    } */



   /*
    public static ArrayList<BusinessProfil> parseProfilesResponse(JSONObject result) {


        ArrayList<BusinessProfil> Profils = new ArrayList<BusinessProfil>();
        try {
            JSONArray profiles = result.getJSONArray(AppConstants.profiles);

            for(int i=0;i<profiles.length();i++)
            {

              JSONObject item = (JSONObject) profiles.get(i);

                BusinessProfil prof = new BusinessProfil(item.getString(AppConstants.profile_id),
                        item.getString(AppConstants.region_id), item.getString(AppConstants.type), item.getString(AppConstants.name));

                try {
                    JSONObject profile = (JSONObject) item.get(AppConstants.profile);
                    prof.setCategory_id(profile.getString(AppConstants.category_id));
                    prof.setName(profile.getString(AppConstants.name));
                    prof.setPhone(profile.getString(AppConstants.phone));
                    prof.setFax(profile.getString(AppConstants.fax));
                    prof.setMobile(profile.getString(AppConstants.mobile));
                    prof.setAddress(profile.getString(AppConstants.address));
                    prof.setOpening_days(profile.getString(AppConstants.opening_days));
                    prof.setOpening_time(profile.getString(AppConstants.opening_time));
                    prof.setClosing_time(profile.getString(AppConstants.closing_time));
                    prof.setLatitude(profile.getString(AppConstants.latitude));
                    prof.setLongitude(profile.getString(AppConstants.longitude));
                    prof.setStars(profile.getString(AppConstants.stars));
                    prof.setUser_id(profile.getString(AppConstants.user_id));
                }catch (JSONException e) {

                    e.printStackTrace();
                }

                Profils.add(prof);
            }

            }

        catch (JSONException e) {

            e.printStackTrace();
        }

        return Profils;


} */


    public static ArrayList<Region> parseRegionResponse(JSONObject result) {


        ArrayList<Region> Regions = new ArrayList<Region>();
        try {
            JSONArray profiles = result.getJSONArray(AppConstants.regions);

            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);

                Region reg = new Region(item.getString(AppConstants.id), item.getString(AppConstants.name));

                Regions.add(reg);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Regions;


    }

    public static ArrayList<Category> parseCategoryResponse(JSONObject result) {


        ArrayList<Category> Categorys = new ArrayList<Category>();
        try {
            JSONArray profiles = result.getJSONArray(AppConstants.categories);

            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);

                Category reg = new Category(item.getString(AppConstants.id), item.getString(AppConstants.name));

                Categorys.add(reg);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Categorys;


    }

    public static ArrayList<Type> parseTypeResponse(JSONObject result) {


        ArrayList<Type> Types = new ArrayList<Type>();
        try {
            JSONArray profiles = result.getJSONArray(AppConstants.id_types);

            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);

                Type reg = new Type(item.getString(AppConstants.id), item.getString(AppConstants.name));

                Types.add(reg);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Types;


    }

    public static int ParseFacebookResponse(JSONObject graphObject, String key) {


        int total = 0;

        if (graphObject == null)
            return total;

        try {
            total = graphObject.getInt(key);
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return total;


    }


    public static ArrayList<BusinessProfil> parseBusinessProfilesResponse(JSONObject result) {


        ArrayList<BusinessProfil> Profils = new ArrayList<BusinessProfil>();
        try {
            JSONObject profiles = result.getJSONObject(AppConstants.profiles);
            JSONArray data = profiles.getJSONArray(AppConstants.data);

            for (int i = 0; i < data.length(); i++) {

                JSONObject item = (JSONObject) data.get(i);

                BusinessProfil prof = new BusinessProfil(item.getString(AppConstants.id), item.getString(AppConstants.user_id),
                        item.getString(AppConstants.region_id),
                        item.getString(AppConstants.category_id), item.getString(AppConstants.type),
                        item.getString(AppConstants.phone),
                        item.getString(AppConstants.fax), item.getString(AppConstants.mobile), item.getString(AppConstants.address),
                        item.getString(AppConstants.opening_days), item.getString(AppConstants.opening_time),
                        item.getString(AppConstants.closing_time), item.getString(AppConstants.latitude),
                        item.getString(AppConstants.longitude), item.getString(AppConstants.stars));

                prof.setName(item.getString(AppConstants.name));
                User user = new User();
                JSONObject userObject = (JSONObject) item.get(AppConstants.user);
                user.setAvatar(userObject.getString(AppConstants.avatar));
                prof.setUser(user);
                prof.setOwner(item.getString(AppConstants.owner));
                prof.setUuid(item.getString(AppConstants.uuid));
                prof.setGender(item.getString(AppConstants.gender));
                prof.setConsultation_fee(item.getString(AppConstants.consultation_fee));
                prof.setViewable(item.getString(AppConstants.viewable));
                prof.setIs_featured(item.getString(AppConstants.is_featured));
                if (item.has("description")) {
                    prof.setDescription(item.getString(AppConstants.description));
                } if (item.has("speciality_details")) {
                    prof.setSpeciality_details(item.getString("speciality_details"));
                }

                Profils.add(prof);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Profils;


    }


    public static ArrayList<BusinessProfil> parseFavorisProfilesResponse(JSONArray profiles) {

        ArrayList<BusinessProfil> Profils = new ArrayList<BusinessProfil>();
        try {
            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);

                BusinessProfil prof = new BusinessProfil(item.getString(AppConstants.profile_id),
                        "", item.getString(AppConstants.category), "");

                prof.setFavorisId(item.getString(AppConstants.id));

                try {
                    JSONObject profile = (JSONObject) item.get(AppConstants.profile);
                    prof.setCategory_id(profile.getString(AppConstants.category_id));
                    prof.setName(profile.getString(AppConstants.name));
                    User user = new User();
                    JSONObject userObject = (JSONObject) profile.get(AppConstants.user);
                    if(userObject.has(AppConstants.avatar))
                    {user.setAvatar(userObject.getString(AppConstants.avatar));}
                    prof.setUser(user);
                    prof.setPhone(profile.getString(AppConstants.phone));
                    prof.setFax(profile.getString(AppConstants.fax));
                    prof.setMobile(profile.getString(AppConstants.mobile));
                    prof.setAddress(profile.getString(AppConstants.address));
                    prof.setOpening_days(profile.getString(AppConstants.opening_days));
                    prof.setOpening_time(profile.getString(AppConstants.opening_time));
                    prof.setClosing_time(profile.getString(AppConstants.closing_time));
                    prof.setLatitude(profile.getString(AppConstants.latitude));
                    prof.setLongitude(profile.getString(AppConstants.longitude));
                    prof.setStars(profile.getString(AppConstants.stars));
                    prof.setRegion_id(profile.getString(AppConstants.region_id));
                    prof.setUser_id(profile.getString(AppConstants.user_id));
                    prof.setConsultation_fee(profile.getString(AppConstants.consultation_fee));
                    if (profile.has("description")) {
                        prof.setDescription(profile.getString(AppConstants.description));
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

                Profils.add(prof);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Profils;


    }


    public static ArrayList<String> parseFavorisIdResponse(JSONArray profiles) {


        ArrayList<String> Profils = new ArrayList<String>();
        try {
            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);
                Profils.add(item.getString(AppConstants.profile_id));

            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Profils;


    }


    public static ArrayList<Category> parseAdviceCategoryResponse(JSONObject result) {


        ArrayList<Category> Categorys = new ArrayList<Category>();
        try {
            JSONArray profiles = result.getJSONArray(AppConstants.data);

            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);

                Category reg = new Category(item.getString(AppConstants.id), item.getString(AppConstants.name));

                Categorys.add(reg);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Categorys;


    }


    public static ArrayList<Comment> parseCommentResponse(JSONObject result) {


        ArrayList<Comment> Categorys = new ArrayList<Comment>();
        try {
            JSONArray comments = result.getJSONArray(AppConstants.comments);

            for (int i = 0; i < comments.length(); i++) {

                JSONObject item = (JSONObject) comments.get(i);


                Comment reg = new Comment(item.getString(AppConstants.id), item.getString(AppConstants.comment), item.getString(AppConstants.updated_at));

                if (item.has(AppConstants.author)) {
                    JSONObject author = (JSONObject) item.get(AppConstants.author);
                    reg.setAuthorName(author.getString(AppConstants.name));
                    reg.setAvatar(author.getString(AppConstants.avatar));
                }
                Categorys.add(reg);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Categorys;


    }


    public static ArrayList<Article> parseArticleResponse(JSONObject result) {


        ArrayList<Article> Articles = new ArrayList<Article>();
        try {
            JSONObject data = (JSONObject) result.get(AppConstants.data);
            JSONArray profiles = data.getJSONArray(AppConstants.data);

            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);

                Article article = new Article(item.getString(AppConstants.id), item.getString(AppConstants.uuid), item.getString(AppConstants.user_id));
                article.setCategory_id(item.getString(AppConstants.category_id));
                article.setTitle(item.getString(AppConstants.title));
                article.setBody(item.getString(AppConstants.body));
                article.setRead_count(item.getString(AppConstants.read_count));
                article.setAllow_comments(item.getString(AppConstants.allow_comments));
                article.setCreated_at(item.getString(AppConstants.created_at));
                try {
                    Author author = new Author();
                    JSONObject profile = (JSONObject) item.get(AppConstants.author);
                    author.setId(profile.getInt(AppConstants.id));
                    author.setName(profile.getString(AppConstants.name));
                    author.setMobile(profile.getString(AppConstants.mobile));
                    author.setAvatar(profile.getString(AppConstants.avatar));
                    article.setAuthor(author);
                } catch (JSONException e) {

                    e.printStackTrace();
                }

                Articles.add(article);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Articles;


    }

    public static String parseArticleNbView(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            JSONObject data = jObjectComment.getJSONObject(AppConstants.data);
            status_logging = data.getString(AppConstants.read_count);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }


    public static ArrayList<Promotions> parsePromotionsResponse(JSONArray profiles) {

        ArrayList<Promotions> Promotions = new ArrayList<Promotions>();
        try {
            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);

                Promotions promo = new Promotions(item.getString(AppConstants.id), item.getString(AppConstants.profile_id), item.getString(AppConstants.category),
                        item.getString(AppConstants.start_date), item.getString(AppConstants.end_date),
                        item.getString(AppConstants.title), item.getString(AppConstants.body), item.getString(AppConstants.publish), item.getString(AppConstants.pic));

                Promotions.add(promo);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Promotions;


    }


    public static ArrayList<Reservation> parseReservationResponse(JSONObject result) {

        ArrayList<Reservation> Reservations = new ArrayList<Reservation>();
        try {
            JSONArray profiles = result.getJSONArray(AppConstants.data);
            for (int i = 0; i < profiles.length(); i++) {

                JSONObject item = (JSONObject) profiles.get(i);

                Reservation promo = new Reservation(item.getString(AppConstants.id), item.getString(AppConstants.profile_id),
                        item.getString(AppConstants.user_id), item.getString(AppConstants.requested_date),
                        item.getString(AppConstants.requested_time), item.getString(AppConstants.notes),
                        item.getString(AppConstants.confirmed), item.getString(AppConstants.rejected));


                Reservations.add(promo);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Reservations;


    }


    public static Profil parseLoggingProfileResponse(JSONObject jObjectComment) {
        JSONObject profileinfos;
        JSONObject user;
        Profil profile = new Profil();
        try {
            profileinfos = jObjectComment.getJSONObject(AppConstants.profile);
            user = profileinfos.getJSONObject(AppConstants.user);

            if (profileinfos.has(AppConstants.region_id)) {
                profile.setRegion_id(profileinfos.getString(AppConstants.region_id));
            }
            if (profileinfos.has(AppConstants.user_id)) {
                profile.setUserId(profileinfos.getString(AppConstants.user_id));
            }
            if (profileinfos.has(AppConstants.address)) {
                profile.setAddress(profileinfos.getString(AppConstants.address));
            }

            if (profileinfos.has(AppConstants.gender)) {
                profile.setGender(profileinfos.getString(AppConstants.gender));
            }

            if (profileinfos.has(AppConstants.mobile)) {
                profile.setMobile(profileinfos.getString(AppConstants.mobile));
            }

            if (profileinfos.has(AppConstants.fax)) {
                profile.setFax(profileinfos.getString(AppConstants.fax));
            }

            if (profileinfos.has(AppConstants.age)) {
                profile.setAge(profileinfos.getString(AppConstants.age));
            }

            if (profileinfos.has(AppConstants.avatar)) {
                profile.setAvatar(profileinfos.getString(AppConstants.avatar));
            }
            if (user.has(AppConstants.name)) {
                profile.setName(user.getString(AppConstants.name));
            }
            if (user.has(AppConstants.avatar)) {
                profile.setPic(user.getString(AppConstants.avatar));
            }

            if (user.has(AppConstants.id_type_id)) {
                profile.setId_type_id(user.getString(AppConstants.id_type_id));
            }

            if (user.has(AppConstants.id_number)) {
                profile.setId_number(user.getString(AppConstants.id_number));
            }


        } catch (JSONException e) {

            e.printStackTrace();
        }
        return profile;
    }


    public static ArrayList<Med> parseMedResponse(JSONObject result) {


        ArrayList<Med> Categorys = new ArrayList<Med>();
        JSONArray comments = null;
        try {
            comments = result.getJSONArray(AppConstants.medicines);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (comments == null) {
            try {
                JSONObject medicines = result.getJSONObject(AppConstants.medicines);
                comments = medicines.getJSONArray(AppConstants.data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        try {


            for (int i = 0; i < comments.length(); i++) {

                JSONObject item = (JSONObject) comments.get(i);

                Med reg = new Med(item.getString(AppConstants.id), item.getString(AppConstants.name), item.getString(AppConstants.description), item.getString(AppConstants.pic));

                Categorys.add(reg);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Categorys;


    }


    public static String parseMedNextPage(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            JSONObject profiles = jObjectComment.getJSONObject(AppConstants.medicines);
            status_logging = profiles.getString(AppConstants.next_page_url);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }


    public static ArrayList<BusinessProfil> parsePharmacyResponse(JSONObject result) {


        ArrayList<BusinessProfil> Profils = new ArrayList<BusinessProfil>();
        try {
            JSONArray data = result.getJSONArray(AppConstants.pharmacies);

            for (int i = 0; i < data.length(); i++) {

                JSONObject item = (JSONObject) data.get(i);

                BusinessProfil prof = new BusinessProfil(item.getString(AppConstants.id), item.getString(AppConstants.user_id),
                        item.getString(AppConstants.region_id),
                        item.getString(AppConstants.category_id), item.getString(AppConstants.type),
                        item.getString(AppConstants.phone),
                        item.getString(AppConstants.fax), item.getString(AppConstants.mobile), item.getString(AppConstants.address),
                        item.getString(AppConstants.opening_days), item.getString(AppConstants.opening_time),
                        item.getString(AppConstants.closing_time), item.getString(AppConstants.latitude),
                        item.getString(AppConstants.longitude), item.getString(AppConstants.stars));

                prof.setName(item.getString(AppConstants.name));
                User user = new User();
                JSONObject userObject = (JSONObject) item.get(AppConstants.user);
                user.setAvatar(userObject.getString(AppConstants.avatar));
                prof.setUser(user);
                prof.setOwner(item.getString(AppConstants.owner));
                prof.setUuid(item.getString(AppConstants.uuid));
                prof.setGender(item.getString(AppConstants.gender));
                prof.setConsultation_fee(item.getString(AppConstants.consultation_fee));
                prof.setViewable(item.getString(AppConstants.viewable));
                prof.setIs_featured(item.getString(AppConstants.is_featured));
                if (item.has("description")) {
                    prof.setDescription(item.getString(AppConstants.description));
                }

                Profils.add(prof);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Profils;


    }


    public static ArrayList<MessageItem> parseListMessageResponse(JSONObject result) {


        ArrayList<MessageItem> Profils = new ArrayList<MessageItem>();
        try {
            JSONArray data = result.getJSONArray(AppConstants.data);

            for (int i = 0; i < data.length(); i++) {

                JSONObject item = (JSONObject) data.get(i);

                MessageItem prof = new MessageItem(item.getString(AppConstants.id), item.getString(AppConstants.uuid),
                        item.getString(AppConstants.sender_id), item.getString(AppConstants.receiver_id),
                        item.getString(AppConstants.subject), item.getString(AppConstants.body),
                        item.getString(AppConstants.read), item.getString(AppConstants.anonymous),
                        item.getString(AppConstants.created_at), item.getString(AppConstants.sender_name),
                        item.getString(AppConstants.sender_avatar), item.getString(AppConstants.recipient_name),
                        item.getString(AppConstants.recipient_avatar));

                prof.setHeader(AppUtility.headerTitle(item.getString(AppConstants.created_at)));
                Profils.add(prof);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Profils;


    }


    public static String parseMessageNextPage(JSONObject data) {
        String status_logging = "";
        try {
            status_logging = data.getString(AppConstants.next_page_url);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }


    public static ArrayList<Notif> parseListNotifsResponse(JSONObject result) {


        ArrayList<Notif> Profils = new ArrayList<Notif>();
        try {
            JSONArray notifications = result.getJSONArray(AppConstants.notifications);

            for (int i = 0; i < notifications.length(); i++) {

                JSONObject item = (JSONObject) notifications.get(i);

                Notif prof = new Notif(item.getString(AppConstants.id), item.getString(AppConstants.type), item.getString(AppConstants.created_at));

                if (prof.getType().equals("App\\Notifications\\ReservationConfirmed")) {

                    JSONObject data = item.getJSONObject(AppConstants.data);
                    if (data.has(AppConstants.notificationType)) {
                        prof.setNotificationType(data.getString(AppConstants.notificationType));
                        prof.setHeader(data.getString(AppConstants.notificationType));
                    }
                    if (data.has(AppConstants.appointmentDate)) {
                        prof.setAppointmentDate(data.getString(AppConstants.appointmentDate));
                    }
                    if (data.has(AppConstants.appointmentTime)) {
                        prof.setAppointmentTime(data.getString(AppConstants.appointmentTime));
                    }
                    if (data.has(AppConstants.appointmentDoctor)) {
                        prof.setAppointmentDoctor(data.getString(AppConstants.appointmentDoctor));
                    }
                    prof.setIdheader(0);
                    Profils.add(prof);
                }


            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return Profils;


    }

    public static BusinessProfil parseSingleBusinessProfilesResponse(JSONObject result) {

        BusinessProfil profil = null;
        try {
            JSONObject item = result.getJSONObject(AppConstants.data);

                BusinessProfil prof = new BusinessProfil(item.getString(AppConstants.id), item.getString(AppConstants.user_id),
                        item.getString(AppConstants.region_id),
                        item.getString(AppConstants.category_id), item.getString(AppConstants.type),
                        item.getString(AppConstants.phone),
                        item.getString(AppConstants.fax), item.getString(AppConstants.mobile), item.getString(AppConstants.address),
                        item.getString(AppConstants.opening_days), item.getString(AppConstants.opening_time),
                        item.getString(AppConstants.closing_time), item.getString(AppConstants.latitude),
                        item.getString(AppConstants.longitude), item.getString(AppConstants.stars));

                prof.setName(item.getString(AppConstants.name));
                User user = new User();
                JSONObject userObject = (JSONObject) item.get(AppConstants.user);
                user.setAvatar(userObject.getString(AppConstants.avatar));
                prof.setUser(user);
                prof.setOwner(item.getString(AppConstants.owner));
                prof.setUuid(item.getString(AppConstants.uuid));
                prof.setGender(item.getString(AppConstants.gender));
                prof.setConsultation_fee(item.getString(AppConstants.consultation_fee));
                prof.setViewable(item.getString(AppConstants.viewable));
                prof.setIs_featured(item.getString(AppConstants.is_featured));
                if (item.has("description")) {
                    prof.setDescription(item.getString(AppConstants.description));
                }
            if (item.has("speciality_details")) {
                prof.setSpeciality_details(item.getString("speciality_details"));
            }

                profil = prof;

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return profil;


    }

    public static String parseNbMessageResponse(JSONObject jObjectComment) {
        String status_logging = "";
        try {
            status_logging = jObjectComment.getString(AppConstants.newmessage);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return status_logging;
    }


}
