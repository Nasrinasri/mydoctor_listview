package com.sahtek.mydoctor;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.utils.AppUtility;

public class ToolsLabDetailActivity extends AppCompatActivity {

    BusinessProfil _profil = null;
    TextView name, sepliciality, description, datetime, phone, mobile, adresse,price;
    LinearLayout topbottomcall, topaction_message, topaction_review, topaction_favoris;
    MapView mMapView;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tools_lab_detail);
        setupActionBar();
        InitialiseMapView(savedInstanceState);

        name = (TextView) findViewById(R.id.name);
        sepliciality = (TextView) findViewById(R.id.sepliciality);
        description = (TextView) findViewById(R.id.description);
        datetime = (TextView) findViewById(R.id.datetime);
        phone = (TextView) findViewById(R.id.phone);
        mobile = (TextView) findViewById(R.id.mobile);
        adresse = (TextView) findViewById(R.id.adresse);
        price = (TextView) findViewById(R.id.price);


        topbottomcall = (LinearLayout) findViewById(R.id.topbottomcall);
        topaction_message = (LinearLayout) findViewById(R.id.topaction_message);
        topaction_review = (LinearLayout) findViewById(R.id.topaction_review);
        topaction_favoris = (LinearLayout) findViewById(R.id.topaction_favoris);
        AddClickListenner();

        Intent mIntent = getIntent();
        _profil = (BusinessProfil) mIntent.getParcelableExtra("profile");
        if (_profil != null) {
            name.setText(_profil.getName());
            sepliciality.setText(_profil.getType());
            description.setText(_profil.getDescription());
            phone.setText(_profil.getPhone() + " ");
            mobile.setText(_profil.getMobile() + " ");
            adresse.setText(_profil.getAddress() + " ");
            datetime.setText(_profil.getOpening_days() + " " + _profil.getOpening_time() + " -> " + _profil.getClosing_time());
            ImageView imageView = findViewById(R.id.img);
            if(_profil.getUser() != null)
            {AppUtility.loadPhoto(this, _profil.getUser().getAvatar(), imageView);}

            if(_profil.getType().equals("laboratory")) {
                price.setText(_profil.getConsultation_fee() + " "+getString(R.string.currency));
                price.setVisibility(View.VISIBLE);
            }

        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            // actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setLogo(R.mipmap.ic_bar);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }

    }
    //MapView configuration

    private void AddClickListenner() {

        topbottomcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtility.makeCall(ToolsLabDetailActivity.this, _profil.getPhone());

            }
        });

        topaction_favoris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtility.AddFavoris(ToolsLabDetailActivity.this, _profil.getId());
            }
        });

        topaction_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMessage dialog = new SendMessage(ToolsLabDetailActivity.this, _profil.getUser_id());
                dialog.show();
            }
        });


        topaction_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateProfile dialog = new RateProfile(ToolsLabDetailActivity.this, _profil.getId());
                dialog.show();
            }
        });

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    private void InitialiseMapView(Bundle savedInstanceState) {
        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                BitmapDescriptor iconPayer = BitmapDescriptorFactory.fromResource(R.drawable.payer_mark);
                LatLng doctorLocation = new LatLng(Float.parseFloat(_profil.getLatitude()), Float.parseFloat(_profil.getLongitude()));
                googleMap.addMarker(new MarkerOptions().position(doctorLocation).title(_profil.getName()).snippet(_profil.getType()));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(doctorLocation).zoom(14).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if (ContextCompat.checkSelfPermission(ToolsLabDetailActivity.this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    googleMap.setMyLocationEnabled(true);


                    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    if (myLocation == null) {
                        Criteria criteria = new Criteria();
                        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                        String provider = lm.getBestProvider(criteria, true);
                        myLocation = lm.getLastKnownLocation(provider);
                    }

                    if (myLocation != null) {

                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.part_marker);
                        LatLng sydney1 = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        googleMap.addMarker(new MarkerOptions().position(sydney1).title("My position").snippet("---").icon(icon));
                    }


                } else {
                    ActivityCompat.requestPermissions(ToolsLabDetailActivity.this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            700);
                }


            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 700: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onResume();

                } else {
                    Log.i("Permission", "Permission denied by user.");
                }
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nmain, menu);
        MenuItem item_message = menu.findItem(R.id.action_message);
        item_message.setActionView(R.layout.messahe_with_count);
        RelativeLayout messageCount = (RelativeLayout) item_message.getActionView();
        TextView messageCountMenuText = messageCount.findViewById(R.id.message_count);
        messageCountMenuText.setText(MainActivity.rsp);
        if(MainActivity.rsp.equals("0")) {
            messageCountMenuText.setVisibility(View.GONE);
        } else {
            messageCountMenuText.setVisibility(View.VISIBLE);
        }


        item_message.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(ToolsLabDetailActivity.this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                AppUtility.showFilterPopup(findViewById(R.id.action_settings), this);
                return true;

            case R.id.action_message:
                Intent ii = new Intent(this, MessageActivity.class);
                startActivity(ii);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;

            case R.id.action_notif:
                Intent notif = new Intent(this, NotificationActivity.class);
                startActivity(notif);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
