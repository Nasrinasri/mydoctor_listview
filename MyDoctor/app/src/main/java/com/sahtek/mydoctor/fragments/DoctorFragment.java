package com.sahtek.mydoctor.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.sahtek.mydoctor.DoctorDetailsActivity;
import com.sahtek.mydoctor.MainActivity;
import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.adapters.ListDoctorAdapter;
import com.sahtek.mydoctor.models.BusinessProfil;
import com.sahtek.mydoctor.utils.AppConstants;
import com.sahtek.mydoctor.utils.AppRestClient;
import com.sahtek.mydoctor.utils.AppUtility;
import com.sahtek.mydoctor.utils.ResponseParser;
import com.sahtek.mydoctor.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class DoctorFragment extends Fragment {

    LinearLayout topaction_latest, topaction_reg, topaction_price, topaction_nearby, topaction_category, topaction_name;
    ListDoctorAdapter adapter;
    ListView doctor_list;
    ArrayList<BusinessProfil> profils = new ArrayList<BusinessProfil>();
    boolean mapShow = false;
    Context _context;
    String type = "doctor";
    String region_id = "";
    String category_id = "";
    String latitude = "";
    String longitude = "";
    String distance = "";
    String name = "";
    String sort_direction = "";
    String sort_key = "";
    ProgressBar pbr;
    String nextPage = "null";
    private boolean loadingMore = false;
    RelativeLayout list_footer;
    boolean first = true;
    EditText search_ed;
    Location myLocation = null;

    public DoctorFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DoctorFragment newInstance(String param1, String param2) {
        DoctorFragment fragment = new DoctorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        _context = getActivity();
        if (AppUtility.isInternetConnected(_context)) {
            GetListDoctor();

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_doctor, container, false);

        InitialiseView(rootView);
        InitialiseMapView(rootView, savedInstanceState);
        AddClickListenner();
        return rootView;
    }


    private void InitialiseView(View rootView) {

        doctor_list = (ListView) rootView.findViewById(R.id.doctor_list);


        topaction_latest = (LinearLayout) rootView.findViewById(R.id.topaction_latest);
        topaction_reg = (LinearLayout) rootView.findViewById(R.id.topaction_reg);
        topaction_price = (LinearLayout) rootView.findViewById(R.id.topaction_price);
        topaction_nearby = (LinearLayout) rootView.findViewById(R.id.topaction_nearby);
        topaction_category = (LinearLayout) rootView.findViewById(R.id.topaction_category);
        topaction_name = (LinearLayout) rootView.findViewById(R.id.topaction_name);
        pbr = (ProgressBar) rootView.findViewById(R.id.pbr);
        list_footer = (RelativeLayout) rootView.findViewById(R.id.list_footer);

        search_ed = (EditText) rootView.findViewById(R.id.search_ed);

        adapter = new ListDoctorAdapter(_context, profils);
        doctor_list.setAdapter(adapter);


    }

    private void AddClickListenner() {

        topaction_latest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (topaction_latest.isActivated()) {
                    sort_key = "";
                    sort_direction = "";
                } else {
                    sort_key = "created_at";
                    sort_direction = "DESC";
                }
                ChangeFiltres();
                Filtredoctor();

            }
        });

        topaction_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PriceDialog();
            }
        });

        topaction_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NameDialog();
            }
        });

        search_ed.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                name = search_ed.getText().toString().trim();
                if(name.isEmpty()){
                    Filtredoctor();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1,
                                      int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
        });

        search_ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Filtredoctor();
                    return true;
                }
                return false;
            }
        });

        topaction_nearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (mapShow) {
                    mMapView.setVisibility(View.GONE);
                    doctor_list.setVisibility(View.VISIBLE);
                    mapShow = false;
                    topaction_nearby.setActivated(false);
                    latitude = "";
                    longitude = "";
                    distance = "";
                    Filtredoctor();

                } else {
                    if (ShowMarker()) {
                        mMapView.setVisibility(View.VISIBLE);
                        doctor_list.setVisibility(View.GONE);
                        mapShow = true;
                        topaction_nearby.setActivated(true);

                    }

                }
            }
        });

        topaction_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegionDialog();
            }
        });

        topaction_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CategoriesDialog();
            }
        });

        doctor_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent ii = new Intent(getActivity(), DoctorDetailsActivity.class);
                BusinessProfil prof = (BusinessProfil) adapter.getItem(i);
                ii.putExtra("profile_id", prof.getId());
                ii.putExtra("profile", prof);
                getActivity().startActivity(ii);
                getActivity().overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        doctor_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int currentFirstVisPos = view.getFirstVisiblePosition();
                if (!nextPage.equals("null") && !nextPage.equals("")) {

                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    if ((lastInScreen == totalItemCount) && !(loadingMore)) {

                        if (AppUtility.isInternetConnected(getActivity())) {
                            loadingMore = true;
                            GetListDoctor();

                        } else {

                            Toast.makeText(getActivity(),
                                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                                    .show();
                        }
                    }


                }

            }
        });
    }

    public void RegionDialog() {

        final String[] items = new String[MainActivity.ListRegion.size() + 1];
        items[0] = _context.getString(R.string.all);
        for (int i = 0; i < MainActivity.ListRegion.size(); i++) {
            items[i + 1] = MainActivity.ListRegion.get(i).getName();
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View titleView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.region_txt));

        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                //received_country_value.setText(items[item]);
                if (item == 0) {
                    region_id = "";
                    topaction_reg.setActivated(false);
                } else {
                    region_id = "" + MainActivity.ListRegion.get(item - 1).getId();
                    topaction_reg.setActivated(true);
                }

                topaction_nearby.setActivated(false);
                mMapView.setVisibility(View.GONE);
                doctor_list.setVisibility(View.VISIBLE);
                mapShow = false;
                latitude = "";
                longitude = "";
                distance = "";

                Filtredoctor();
            }
        }).show();

    }

    private void Filtredoctor() {

        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)_context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        first = true;
        profils.clear();
        adapter.notifyDataSetChanged();
        if (AppUtility.isInternetConnected(_context)) {
            loadingMore = true;
            GetListDoctor();

        } else {
            Toast.makeText(_context,
                    R.string.internet_connection_error_text, Toast.LENGTH_LONG)
                    .show();

        }
    }

    public void CategoriesDialog() {

        final String[] items = new String[MainActivity.ListCategory.size() + 1];
        items[0] = _context.getString(R.string.all);
        for (int i = 0; i < MainActivity.ListCategory.size(); i++) {
            items[i + 1] = MainActivity.ListCategory.get(i).getName();
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View titleView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.action_category));

        builder.setCustomTitle(titleView);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                //received_country_value.setText(items[item]);
                if (item == 0) {
                    category_id = "";
                    topaction_category.setActivated(false);
                } else {
                    category_id = "" + MainActivity.ListCategory.get(item - 1).getId();
                    topaction_category.setActivated(true);
                }
                Filtredoctor();
            }
        }).show();

    }

    private void PriceDialog() {

        final CharSequence[] day_radio = {_context.getString(R.string.ordecroissant), _context.getString(R.string.orderdecroissant), _context.getString(R.string.ordreclose)};

        View titleView = LayoutInflater.from(_context).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.ordretitle));

        AlertDialog.Builder builder2 = new AlertDialog.Builder(_context)
                .setCustomTitle(titleView)
                .setItems(day_radio, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        boolean refresh = true;
                        if (item == 0) {
                            sort_key = "consultation_fee";
                            sort_direction = "ASC";
                        } else if (item == 1) {
                            sort_key = "consultation_fee";
                            sort_direction = "DESC";
                        } else {
                            if (topaction_price.isActivated()) {
                                sort_key = "";
                                sort_direction = "";
                            } else {
                                refresh = false;
                            }

                        }
                        if (refresh) {
                            ChangeFiltres();
                            Filtredoctor();
                        }

                    }
                });
        AlertDialog alertdialog2 = builder2.create();
        alertdialog2.show();

    }

    private void NameDialog() {

        final CharSequence[] day_radio = {_context.getString(R.string.ordecroissant), _context.getString(R.string.orderdecroissant), _context.getString(R.string.ordreclose)};

        View titleView = LayoutInflater.from(_context).inflate(R.layout.dialog_custom_title, null);
        TextView title = titleView.findViewById(R.id.tv_dialog_title);
        title.setText(getString(R.string.ordretitle));

        AlertDialog.Builder builder2 = new AlertDialog.Builder(_context)
                .setCustomTitle(titleView)
                .setItems(day_radio, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        boolean refresh = true;
                        if (item == 0) {
                            sort_key = "name";
                            sort_direction = "ASC";
                        } else if (item == 1) {
                            sort_key = "name";
                            sort_direction = "DESC";
                        } else {
                            if (topaction_name.isActivated()) {
                                sort_key = "";
                                sort_direction = "";
                            } else {
                                refresh = false;
                            }
                        }
                        if (refresh) {
                            ChangeFiltres();
                            Filtredoctor();
                        }
                    }
                });
        AlertDialog alertdialog2 = builder2.create();
        alertdialog2.show();

    }

    private void ChangeFiltres() {
        topaction_latest.setActivated(false);
        topaction_price.setActivated(false);
        topaction_name.setActivated(false);

        if (sort_key.equals("consultation_fee")) {
            topaction_price.setActivated(true);

        } else if (sort_key.equals("created_at")) {
            topaction_latest.setActivated(true);

        } else if (sort_key.equals("name")) {
            topaction_name.setActivated(true);
        }
    }

    //MapView configuration

    MapView mMapView;
    private GoogleMap googleMap;

    private void InitialiseMapView(View rootView, Bundle savedInstanceState) {
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button

                if (ContextCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    googleMap.setMyLocationEnabled(true);


                    LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                        @Override
                        public void onMyLocationChange(Location arg0) {
                            // TODO Auto-generated method stub
                            myLocation = arg0;
                        }
                    });

                } else {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            700);
                }


            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    private boolean ShowMarker() {

        boolean actived = false;

        if (googleMap != null) {

            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                if (myLocation != null) {

                    LatLng userLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(userLocation).zoom(14).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    actived = true;
                    latitude = "" + myLocation.getLatitude();
                    longitude = "" + myLocation.getLongitude();
                    distance = "1000";
                    region_id = "";
                    topaction_reg.setActivated(false);

                    Filtredoctor();
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(_context);
                    builder1.setMessage(_context.getString(R.string.gpspb));
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(
                            _context.getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        700);
            }
        }
        return actived;
    }


    private void GetListDoctor() {
        String Connect_URL = "";

        if (first) {
            Connect_URL = AppRestClient.BASE_URL + AppConstants.FEATURED_URL;
            Connect_URL = Connect_URL + "?type=" + type + "&region_id=" +
                    region_id + "&category_id=" +
                    category_id + "&latitude=" + latitude + "&longitude=" + longitude + "&distance=" + distance + "&name=" + name + "&sort_key=" + sort_key + "&sort_direction=" + sort_direction;
        } else {
            Connect_URL = nextPage;
        }


        AppRestClient.getFirst(Connect_URL, new SessionManager(getActivity()).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
            }

            @Override
            public void onStart() {

                if (first) {
                    pbr.setVisibility(View.VISIBLE);
                } else {
                    list_footer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                list_footer.setVisibility(View.GONE);
                first = false;
                loadingMore = false;
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                nextPage = ResponseParser.parseNextPage(response);
                if (rsp.equals("+OK")) {

                    profils.addAll(ResponseParser.parseBusinessProfilesResponse(response));
                    adapter.notifyDataSetChanged();
                    googleMap.clear();
                    if (mapShow) {
                        for (BusinessProfil profil : profils) {
                            LatLng sydney1 = new LatLng(AppUtility.parseFloat(profil.getLatitude()), AppUtility.parseFloat(profil.getLongitude()));
                            googleMap.addMarker(new MarkerOptions().position(sydney1).title(profil.getName()).snippet(profil.getType()));
                        }

                    }


                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }


    private void GetListFavoris() {
        String Connect_URL = AppConstants.GET_FAVORIS_URL + "?category=doctor";

        AppRestClient.get(Connect_URL, new SessionManager(getActivity()).getAccessToken(), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (errorResponse != null) {
                    Toast.makeText(_context,
                            errorResponse.toString(), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    Toast.makeText(_context,
                            _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                            .show();
                }
                pbr.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String aa, Throwable throwable) {
                Toast.makeText(_context,
                        _context.getString(R.string.connexion_problem_get), Toast.LENGTH_SHORT)
                        .show();
                pbr.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onStart() {

                pbr.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pbr.setVisibility(View.INVISIBLE);
                Log.e("response", "response " + response.toString());
                String rsp = ResponseParser.parseResultResponse(response);
                String msg = ResponseParser.parseMessageResponse(response);
                // nextPage = ResponseParser.parseNextPage(response);
                if (rsp.equals("+OK")) {
                    try {
                        JSONObject data = response.getJSONObject(AppConstants.data);
                        JSONArray profiles = data.getJSONArray(AppConstants.doctor);
                        //  profils.addAll(ResponseParser.parseFavorisProfilesResponse(profiles));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(_context,
                            msg, Toast.LENGTH_SHORT)
                            .show();
                }
            }

        });

    }


}
