package com.sahtek.mydoctor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.models.Med;
import com.sahtek.mydoctor.utils.AppUtility;

import java.util.ArrayList;

/**
 * Created by nasri on 29/09/2017.
 */

public class ListMedAdapter extends BaseAdapter {
    Context _context;
    ArrayList<Med> _listItems = new ArrayList<Med>();


    public ListMedAdapter(Context context, ArrayList<Med> _listItems) {
        this._context = context;
        this._listItems = _listItems;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this._listItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this._listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Med med = (Med) getItem(position);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = infalInflater.inflate(R.layout.med_listview_item, null);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        ImageView imageView = convertView.findViewById(R.id.img);

        AppUtility.loadPhoto(_context, med.getPic(), imageView);
        name.setText(med.getName());
        description.setText(med.getDescription());

        return convertView;
    }


}

