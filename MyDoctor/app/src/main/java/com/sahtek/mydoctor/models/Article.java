package com.sahtek.mydoctor.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nasri on 11/01/2018.
 */

public class Article implements Parcelable {


    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };
    private String id = "";
    private String uuid = "";
    private String user_id = "";
    private String category_id = "";
    private String title = "";
    private String body = "";
    private String read_count = "";
    private String mobile = "";
    private String allow_comments = "";
    private String created_at = "";
    private String updated_at = "";
    private String stars = "";
    private String comment_count = "";
    private Author author;

    protected Article(Parcel in) {
        id = in.readString();
        uuid = in.readString();
        user_id = in.readString();
        category_id = in.readString();
        title = in.readString();
        body = in.readString();
        read_count = in.readString();
        mobile = in.readString();
        allow_comments = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        stars = in.readString();
        comment_count = in.readString();
        author = in.readParcelable(Author.class.getClassLoader());
    }

    public Article(String id, String uuid, String user_id, String category_id, String title, String body, String read_count, String allow_comments, String created_at, String updated_at, String stars, String comment_count, Author author) {
        this.id = id;
        this.uuid = uuid;
        this.user_id = user_id;
        this.category_id = category_id;
        this.title = title;
        this.body = body;
        this.read_count = read_count;
        this.allow_comments = allow_comments;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.stars = stars;
        this.comment_count = comment_count;
        this.author = author;
    }

    public Article(String id, String uuid, String user_id) {
        this.id = id;
        this.uuid = uuid;
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRead_count() {
        return read_count;
    }

    public void setRead_count(String read_count) {
        this.read_count = read_count;
    }

    public String getAllow_comments() {
        return allow_comments;
    }

    public void setAllow_comments(String allow_comments) {
        this.allow_comments = allow_comments;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(uuid);
        parcel.writeString(user_id);
        parcel.writeString(category_id);
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(read_count);
        parcel.writeString(mobile);
        parcel.writeString(allow_comments);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
        parcel.writeString(stars);
        parcel.writeString(comment_count);
        parcel.writeParcelable(author, i);
    }
}
