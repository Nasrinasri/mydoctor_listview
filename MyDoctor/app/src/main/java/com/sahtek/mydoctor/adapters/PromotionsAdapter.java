package com.sahtek.mydoctor.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sahtek.mydoctor.R;
import com.sahtek.mydoctor.models.Promotions;
import com.sahtek.mydoctor.utils.AppUtility;

import java.util.ArrayList;

/**
 * Created by nasri on 13/01/2018.
 */

public class PromotionsAdapter extends BaseAdapter {
    Context _context;
    ArrayList<Promotions> _listItems = new ArrayList<Promotions>();


    public PromotionsAdapter(Context context, ArrayList<Promotions> _listItems) {
        this._context = context;
        this._listItems = _listItems;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this._listItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this._listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Promotions promo = (Promotions) getItem(position);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = infalInflater.inflate(R.layout.promotion_item, null);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView body = (TextView) convertView.findViewById(R.id.body);
        TextView date = (TextView) convertView.findViewById(R.id.date);

        name.setText(promo.getAuthorname());
        title.setText(promo.getTitle());
        date.setText(promo.getStart_date()+" -> "+promo.getEnd_date());
        body.setText(promo.getBody().replaceAll("<p>","").replaceAll("</p>",""));

        ImageView imageView = (ImageView) convertView.findViewById(R.id.img);
        AppUtility.loadPhoto(_context, promo.getPic(), imageView);

        return convertView;
    }




}
