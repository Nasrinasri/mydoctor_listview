package com.sahtek.mydoctor.models;

/**
 * Created by nasri on 18/01/2018.
 */

public class Profil {


    private String region_id = "";
    private String address = "";
    private String gender = "";
    private String fax = "";
    private String mobile = "";
    private String age = "";
    private String avatar = "";
    private String name = "";
    private String pic = "";
    private String id_type_id = "";
    private String id_number = "";
    private String user_id = "";

    public Profil() {
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getAddress() {
        return address.equalsIgnoreCase("null") ? "" : address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFax() {
        return fax.equalsIgnoreCase("null") ? "" : fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId_type_id() {
        return id_type_id;
    }

    public void setId_type_id(String id_type_id) {
        this.id_type_id = id_type_id;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }
}
